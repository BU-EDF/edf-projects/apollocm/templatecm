EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Command_Module_RevB:ERM8-013-01-X-D-RA-DS-L-X-XX J3
U 1 1 61F997D9
P 10500 3200
F 0 "J3" H 10500 4067 50  0000 C CNN
F 1 "ERM8-013-01-X-D-RA-DS-L-X-XX" H 10500 3976 50  0000 C CNN
F 2 "SAMTEC_ERM8-013-01-X-D-RA-DS-L-X-XX" H 11150 2200 50  0001 L BNN
F 3 "https://suddendocs.samtec.com/productspecs/erm8-erf8.pdf" H 10500 3200 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 11150 2100 50  0001 L BNN "STANDARD"
F 5 "6.25mm" H 11500 2000 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "" H 11150 2000 50  0001 L BNN "MANUFACTURER"
F 7 "J" H 10500 3200 50  0001 L BNN "PARTREV"
F 8 "$8.89" H 10500 3200 50  0001 C CNN "Item Cost"
F 9 "ERM8-013-01-L-D-RA-DS" H 10500 3200 50  0001 C CNN "MFN"
F 10 "Samtec Inc." H 10500 3200 50  0001 C CNN "Manufacturer"
F 11 "Digi-Key" H 10500 3200 50  0001 C CNN "Supplier"
F 12 "SAM13696DKR-ND" H 10500 3200 50  0001 C CNN "Supplier P/N"
F 13 "https://www.digikey.com/en/products/detail/samtec-inc/ERM8-013-01-L-D-RA-K-TR/6694644?s=N4IgTCBcDa4AxwLQFEBKBZAHHAjAZlwBkARVAQQGkAVVEAXQF8g" H 10500 3200 50  0001 C CNN "Supplier Link"
	1    10500 3200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 61FB8F51
P 12900 7800
F 0 "H1" H 13000 7846 50  0000 L CNN
F 1 "MountingHole" H 13000 7755 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 12900 7800 50  0001 C CNN
F 3 "~" H 12900 7800 50  0001 C CNN
	1    12900 7800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 61FBD0CE
P 13900 7800
F 0 "H3" H 14000 7846 50  0000 L CNN
F 1 "MountingHole" H 14000 7755 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 13900 7800 50  0001 C CNN
F 3 "~" H 13900 7800 50  0001 C CNN
	1    13900 7800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 61FBDE97
P 12900 8300
F 0 "H2" H 13000 8346 50  0000 L CNN
F 1 "MountingHole" H 13000 8255 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 12900 8300 50  0001 C CNN
F 3 "~" H 12900 8300 50  0001 C CNN
	1    12900 8300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 61FBDEA9
P 13900 8300
F 0 "H4" H 14000 8346 50  0000 L CNN
F 1 "MountingHole" H 14000 8255 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 13900 8300 50  0001 C CNN
F 3 "~" H 13900 8300 50  0001 C CNN
	1    13900 8300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 61FC0809
P 14900 7800
F 0 "H5" H 15000 7846 50  0000 L CNN
F 1 "MountingHole" H 15000 7755 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 14900 7800 50  0001 C CNN
F 3 "~" H 14900 7800 50  0001 C CNN
	1    14900 7800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 61FC0813
P 14900 8300
F 0 "H6" H 15000 8346 50  0000 L CNN
F 1 "MountingHole" H 15000 8255 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 14900 8300 50  0001 C CNN
F 3 "~" H 14900 8300 50  0001 C CNN
	1    14900 8300
	1    0    0    -1  
$EndComp
$Comp
L Command_Module_RevB:ET60T-D02-3-08-000-X-R1-S J4
U 1 1 61F99823
P 14500 3150
F 0 "J4" H 14500 4500 50  0000 C CNN
F 1 "ET60T-D02-3-08-000-X-R1-S" H 14500 4400 50  0000 C CNN
F 2 "Command_Module:SAMTEC_ET60T-D02-3-08-000-X-R1-S_RevB" H 15050 2100 50  0001 L BNN
F 3 "https://suddendocs.samtec.com/productspecs/et60s-et60t.pdf" H 14500 3150 50  0001 L BNN
F 4 "10 mm" H 15350 1900 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 5 "Manufacturer Recommendations" H 15050 2000 50  0001 L BNN "STANDARD"
F 6 "J" H 14500 3150 50  0001 L BNN "PARTREV"
F 7 "" H 15050 1900 50  0001 L BNN "MANUFACTURER"
F 8 "$9.52" H 14500 3150 50  0001 C CNN "Item Cost"
F 9 "ET60T-D02-3-08-000-L-R1-S" H 14500 3150 50  0001 C CNN "MFN"
F 10 "Samtec Inc." H 14500 3150 50  0001 C CNN "Manufacturer"
F 11 "Digi-Key" H 14500 3150 50  0001 C CNN "Supplier"
F 12 "SAM15977-ND" H 14500 3150 50  0001 C CNN "Supplier P/N"
F 13 "https://www.digikey.com/en/products/detail/samtec-inc/ET60T-D02-3-08-000-L-R1-S/10064502?s=N4IgTCBcDaIMoEECyBGArATgOxYLQDkAREAXQF8g" H 14500 3150 50  0001 C CNN "Supplier Link"
	1    14500 3150
	1    0    0    -1  
$EndComp
Text Label 5300 3300 0    50   ~ 0
J2_TMS
Text Label 5300 3400 0    50   ~ 0
J2_TDO
Wire Wire Line
	14000 3000 13800 3000
Wire Wire Line
	13800 3000 13800 2900
Wire Wire Line
	14000 2100 13800 2100
Connection ~ 13800 2100
Wire Wire Line
	13800 2100 13800 2050
Wire Wire Line
	14000 2200 13800 2200
Connection ~ 13800 2200
Wire Wire Line
	13800 2200 13800 2100
Wire Wire Line
	14000 2300 13800 2300
Connection ~ 13800 2300
Wire Wire Line
	13800 2300 13800 2200
Wire Wire Line
	14000 2400 13800 2400
Connection ~ 13800 2400
Wire Wire Line
	13800 2400 13800 2300
Wire Wire Line
	14000 2500 13800 2500
Connection ~ 13800 2500
Wire Wire Line
	13800 2500 13800 2400
Wire Wire Line
	14000 2600 13800 2600
Connection ~ 13800 2600
Wire Wire Line
	13800 2600 13800 2500
Wire Wire Line
	14000 2700 13800 2700
Connection ~ 13800 2700
Wire Wire Line
	13800 2700 13800 2600
Wire Wire Line
	14000 2800 13800 2800
Connection ~ 13800 2800
Wire Wire Line
	13800 2800 13800 2700
Wire Wire Line
	14000 2900 13800 2900
Connection ~ 13800 2900
Wire Wire Line
	13800 2900 13800 2800
$Comp
L power:+12V #PWR0105
U 1 1 622453D0
P 13800 2050
F 0 "#PWR0105" H 13800 1900 50  0001 C CNN
F 1 "+12V" H 13650 2100 50  0000 C CNN
F 2 "" H 13800 2050 50  0001 C CNN
F 3 "" H 13800 2050 50  0001 C CNN
	1    13800 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 4100 13950 4100
Wire Wire Line
	13950 4100 13950 4200
Wire Wire Line
	14000 4200 13950 4200
Connection ~ 13950 4200
Wire Wire Line
	13950 4200 13950 4400
$Comp
L power:GND #PWR0106
U 1 1 622453F3
P 13950 4400
F 0 "#PWR0106" H 13950 4150 50  0001 C CNN
F 1 "GND" H 14050 4400 50  0000 C CNN
F 2 "" H 13950 4400 50  0001 C CNN
F 3 "" H 13950 4400 50  0001 C CNN
	1    13950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	15000 2100 15200 2100
Wire Wire Line
	15200 2100 15200 2200
Wire Wire Line
	15000 3000 15200 3000
Connection ~ 15200 3000
Wire Wire Line
	15200 3000 15200 3050
Wire Wire Line
	15000 2900 15200 2900
Connection ~ 15200 2900
Wire Wire Line
	15200 2900 15200 3000
Wire Wire Line
	15000 2800 15200 2800
Connection ~ 15200 2800
Wire Wire Line
	15200 2800 15200 2900
Wire Wire Line
	15000 2700 15200 2700
Connection ~ 15200 2700
Wire Wire Line
	15200 2700 15200 2800
Wire Wire Line
	15000 2600 15200 2600
Connection ~ 15200 2600
Wire Wire Line
	15200 2600 15200 2700
Wire Wire Line
	15000 2500 15200 2500
Connection ~ 15200 2500
Wire Wire Line
	15200 2500 15200 2600
Wire Wire Line
	15000 2400 15200 2400
Connection ~ 15200 2400
Wire Wire Line
	15200 2400 15200 2500
Wire Wire Line
	15000 2300 15200 2300
Connection ~ 15200 2300
Wire Wire Line
	15200 2300 15200 2400
Wire Wire Line
	15000 2200 15200 2200
Connection ~ 15200 2200
Wire Wire Line
	15200 2200 15200 2300
$Comp
L power:GND #PWR0107
U 1 1 62255D9E
P 15200 3050
F 0 "#PWR0107" H 15200 2800 50  0001 C CNN
F 1 "GND" H 15300 3050 50  0000 C CNN
F 2 "" H 15200 3050 50  0001 C CNN
F 3 "" H 15200 3050 50  0001 C CNN
	1    15200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	15000 3200 15050 3200
Wire Wire Line
	15050 3200 15050 3400
Wire Wire Line
	15000 3400 15050 3400
Connection ~ 15050 3400
Wire Wire Line
	15050 3400 15050 4400
$Comp
L power:GND #PWR0108
U 1 1 62255DBF
P 15050 4400
F 0 "#PWR0108" H 15050 4150 50  0001 C CNN
F 1 "GND" H 15150 4400 50  0000 C CNN
F 2 "" H 15050 4400 50  0001 C CNN
F 3 "" H 15050 4400 50  0001 C CNN
	1    15050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 6550 4500 6550
Wire Wire Line
	4400 8450 4500 8450
Connection ~ 4500 8450
Wire Wire Line
	4400 8350 4500 8350
Wire Wire Line
	4500 6550 4500 6650
Wire Wire Line
	4500 8350 4500 8450
Connection ~ 4500 8350
Wire Wire Line
	4400 8250 4500 8250
Connection ~ 4500 8250
Wire Wire Line
	4500 8250 4500 8350
Wire Wire Line
	4400 7450 4500 7450
Wire Wire Line
	4400 7550 4500 7550
Wire Wire Line
	4400 7650 4500 7650
Wire Wire Line
	4400 7750 4500 7750
Wire Wire Line
	4400 7850 4500 7850
Wire Wire Line
	4400 7950 4500 7950
Wire Wire Line
	4400 8050 4500 8050
Wire Wire Line
	4400 8150 4500 8150
Wire Wire Line
	4400 6650 4500 6650
Wire Wire Line
	4400 6750 4500 6750
Wire Wire Line
	4400 6850 4500 6850
Wire Wire Line
	4400 6950 4500 6950
Wire Wire Line
	4400 7050 4500 7050
Wire Wire Line
	4400 7150 4500 7150
Wire Wire Line
	4400 7250 4500 7250
Wire Wire Line
	4400 7350 4500 7350
Connection ~ 4500 6650
Wire Wire Line
	4500 6650 4500 6750
Connection ~ 4500 6750
Wire Wire Line
	4500 6750 4500 6850
Connection ~ 4500 6850
Wire Wire Line
	4500 6850 4500 6950
Connection ~ 4500 6950
Wire Wire Line
	4500 6950 4500 7050
Connection ~ 4500 7050
Wire Wire Line
	4500 7050 4500 7150
Connection ~ 4500 7150
Wire Wire Line
	4500 7150 4500 7250
Connection ~ 4500 7250
Wire Wire Line
	4500 7250 4500 7350
Connection ~ 4500 7350
Wire Wire Line
	4500 7350 4500 7450
Connection ~ 4500 7450
Wire Wire Line
	4500 7450 4500 7550
Connection ~ 4500 7550
Wire Wire Line
	4500 7550 4500 7650
Connection ~ 4500 7650
Wire Wire Line
	4500 7650 4500 7750
Connection ~ 4500 7750
Wire Wire Line
	4500 7750 4500 7850
Connection ~ 4500 7850
Wire Wire Line
	4500 7850 4500 7950
Connection ~ 4500 7950
Wire Wire Line
	4500 7950 4500 8050
Connection ~ 4500 8050
Wire Wire Line
	4500 8050 4500 8150
Connection ~ 4500 8150
Wire Wire Line
	4500 8150 4500 8250
$Comp
L power:GND #PWR0109
U 1 1 6234511C
P 4500 9000
F 0 "#PWR0109" H 4500 8750 50  0001 C CNN
F 1 "GND" H 4600 9000 50  0000 C CNN
F 2 "" H 4500 9000 50  0001 C CNN
F 3 "" H 4500 9000 50  0001 C CNN
	1    4500 9000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 6550 9200 6550
Wire Wire Line
	9100 8450 9200 8450
Connection ~ 9200 8450
Wire Wire Line
	9100 8350 9200 8350
Wire Wire Line
	9200 6550 9200 6650
Wire Wire Line
	9200 8350 9200 8450
Connection ~ 9200 8350
Wire Wire Line
	9100 8250 9200 8250
Connection ~ 9200 8250
Wire Wire Line
	9200 8250 9200 8350
Wire Wire Line
	9100 7450 9200 7450
Wire Wire Line
	9100 7550 9200 7550
Wire Wire Line
	9100 7650 9200 7650
Wire Wire Line
	9100 7750 9200 7750
Wire Wire Line
	9100 7850 9200 7850
Wire Wire Line
	9100 7950 9200 7950
Wire Wire Line
	9100 8050 9200 8050
Wire Wire Line
	9100 8150 9200 8150
Wire Wire Line
	9100 6650 9200 6650
Wire Wire Line
	9100 6750 9200 6750
Wire Wire Line
	9100 6850 9200 6850
Wire Wire Line
	9100 6950 9200 6950
Wire Wire Line
	9100 7050 9200 7050
Wire Wire Line
	9100 7150 9200 7150
Wire Wire Line
	9100 7250 9200 7250
Wire Wire Line
	9100 7350 9200 7350
Connection ~ 9200 6650
Wire Wire Line
	9200 6650 9200 6750
Connection ~ 9200 6750
Wire Wire Line
	9200 6750 9200 6850
Connection ~ 9200 6850
Wire Wire Line
	9200 6850 9200 6950
Connection ~ 9200 6950
Wire Wire Line
	9200 6950 9200 7050
Connection ~ 9200 7050
Wire Wire Line
	9200 7050 9200 7150
Connection ~ 9200 7150
Wire Wire Line
	9200 7150 9200 7250
Connection ~ 9200 7250
Wire Wire Line
	9200 7250 9200 7350
Connection ~ 9200 7350
Wire Wire Line
	9200 7350 9200 7450
Connection ~ 9200 7450
Wire Wire Line
	9200 7450 9200 7550
Connection ~ 9200 7550
Wire Wire Line
	9200 7550 9200 7650
Connection ~ 9200 7650
Wire Wire Line
	9200 7650 9200 7750
Connection ~ 9200 7750
Wire Wire Line
	9200 7750 9200 7850
Connection ~ 9200 7850
Wire Wire Line
	9200 7850 9200 7950
Connection ~ 9200 7950
Wire Wire Line
	9200 7950 9200 8050
Connection ~ 9200 8050
Wire Wire Line
	9200 8050 9200 8150
Connection ~ 9200 8150
Wire Wire Line
	9200 8150 9200 8250
$Comp
L power:GND #PWR0110
U 1 1 62356F35
P 9200 9000
F 0 "#PWR0110" H 9200 8750 50  0001 C CNN
F 1 "GND" H 9300 9000 50  0000 C CNN
F 2 "" H 9200 9000 50  0001 C CNN
F 3 "" H 9200 9000 50  0001 C CNN
	1    9200 9000
	1    0    0    -1  
$EndComp
Text Label 13300 3200 0    50   ~ 0
J4_PWR_GOOD
Text Label 13300 3300 0    50   ~ 0
J4_TMS
Text Label 13300 3400 0    50   ~ 0
J4_TDO
Text Label 13300 3500 0    50   ~ 0
J4_TX_IPMC
Text Label 13300 3600 0    50   ~ 0
J4_TX_ZYNQ
Text Label 13300 3700 0    50   ~ 0
J4_SENSE_SDA
Text Label 13300 3800 0    50   ~ 0
J4_PWR_EN
Text Label 13300 3900 0    50   ~ 0
J4_FPGA_GPIO0
Text Label 13300 4000 0    50   ~ 0
J4_FPGA_GPIO1
Text Label 13300 4300 0    50   ~ 0
J4_FPGA_GPIO2
Wire Wire Line
	13300 3300 14000 3300
Wire Wire Line
	13300 3400 14000 3400
Wire Wire Line
	13300 3500 14000 3500
Wire Wire Line
	13300 3600 14000 3600
Wire Wire Line
	13300 3700 14000 3700
Wire Wire Line
	13300 3800 14000 3800
Wire Wire Line
	13300 3900 14000 3900
Wire Wire Line
	13300 3200 14000 3200
Wire Wire Line
	13300 4000 14000 4000
Wire Wire Line
	13300 4300 14000 4300
Text Label 15700 3300 2    50   ~ 0
J4_CPLD_GPIO0
Text Label 15700 3500 2    50   ~ 0
J4_CPLD_GPIO1
Text Label 15700 3600 2    50   ~ 0
J4_EN
Text Label 15700 3700 2    50   ~ 0
J4_TCK
Text Label 15700 3800 2    50   ~ 0
J4_TDI
Text Label 15700 3900 2    50   ~ 0
J4_RX_IPMC
Text Label 15700 4000 2    50   ~ 0
J4_RX_ZYNQ
Text Label 15700 4100 2    50   ~ 0
J4_SENSE_SCL
Text Label 15700 4200 2    50   ~ 0
J4_MON_RX
Wire Wire Line
	15000 3500 15700 3500
Wire Wire Line
	15000 3600 15700 3600
Wire Wire Line
	15000 3700 15700 3700
Wire Wire Line
	15000 3800 15700 3800
Wire Wire Line
	15000 3900 15700 3900
Wire Wire Line
	15000 4000 15700 4000
Wire Wire Line
	15000 4100 15700 4100
Wire Wire Line
	15000 4200 15700 4200
Wire Wire Line
	15000 3300 15700 3300
Text Label 3200 7050 0    50   ~ 0
J2_PWR_GOOD
Text Label 3200 7150 0    50   ~ 0
J2_TMS
Text Label 3200 7250 0    50   ~ 0
J2_TDO
Text Label 3200 7350 0    50   ~ 0
J2_TX_IPMC
Text Label 3200 7450 0    50   ~ 0
J2_TX_ZYNQ
Text Label 3200 7550 0    50   ~ 0
J2_SENSE_SDA
Text Label 3200 7650 0    50   ~ 0
J2_PWR_EN
Text Label 3200 7750 0    50   ~ 0
J2_FPGA_GPIO0
Text Label 3200 7850 0    50   ~ 0
J2_FPGA_GPIO1
Text Label 3200 7950 0    50   ~ 0
J2_FPGA_GPIO2
Text Label 3200 8050 0    50   ~ 0
J2_CPLD_GPIO0
Text Label 3200 8150 0    50   ~ 0
J2_CPLD_GPIO1
Text Label 3200 8250 0    50   ~ 0
J2_EN
Text Label 3200 8350 0    50   ~ 0
J2_TCK
Text Label 3200 8450 0    50   ~ 0
J2_TDI
Text Label 3200 8550 0    50   ~ 0
J2_RX_IPMC
Text Label 3200 8650 0    50   ~ 0
J2_RX_ZYNQ
Text Label 3200 8750 0    50   ~ 0
J2_SENSE_SCL
Text Label 3200 8850 0    50   ~ 0
J2_MON_RX
Text Label 7900 7050 0    50   ~ 0
J4_PWR_GOOD
Text Label 7900 7150 0    50   ~ 0
J4_TMS
Text Label 7900 7250 0    50   ~ 0
J4_TDO
Text Label 7900 7350 0    50   ~ 0
J4_TX_IPMC
Text Label 7900 7450 0    50   ~ 0
J4_TX_ZYNQ
Text Label 7900 7550 0    50   ~ 0
J4_SENSE_SDA
Text Label 7900 7650 0    50   ~ 0
J4_PWR_EN
Text Label 7900 7750 0    50   ~ 0
J4_FPGA_GPIO0
Text Label 7900 7850 0    50   ~ 0
J4_FPGA_GPIO1
Text Label 7900 7950 0    50   ~ 0
J4_FPGA_GPIO2
Text Label 7900 8050 0    50   ~ 0
J4_CPLD_GPIO0
Text Label 7900 8150 0    50   ~ 0
J4_CPLD_GPIO1
Text Label 7900 8250 0    50   ~ 0
J4_EN
Text Label 7900 8350 0    50   ~ 0
J4_TCK
Text Label 7900 8450 0    50   ~ 0
J4_TDI
Text Label 7900 8550 0    50   ~ 0
J4_RX_IPMC
Text Label 7900 8650 0    50   ~ 0
J4_RX_ZYNQ
Text Label 7900 8750 0    50   ~ 0
J4_SENSE_SCL
Text Label 7900 8850 0    50   ~ 0
J4_MON_RX
$Comp
L Device:R_Small R1
U 1 1 626C4A9D
P 2900 7050
F 0 "R1" V 2850 7150 25  0000 C CNN
F 1 "10k" V 2900 7050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7050 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7050 50  0001 C CNN
F 4 "$0.1" H 2900 7050 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7050 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7050 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7050 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7050 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7050 50  0001 C CNN "Supplier P/N"
	1    2900 7050
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 7950 2600 7950
Wire Wire Line
	2600 7950 2600 7850
Wire Wire Line
	2800 7050 2600 7050
Connection ~ 2600 7050
Wire Wire Line
	2800 7150 2600 7150
Connection ~ 2600 7150
Wire Wire Line
	2600 7150 2600 7050
Wire Wire Line
	2800 7250 2600 7250
Connection ~ 2600 7250
Wire Wire Line
	2600 7250 2600 7150
Wire Wire Line
	2800 7350 2600 7350
Connection ~ 2600 7350
Wire Wire Line
	2600 7350 2600 7250
Wire Wire Line
	2800 7450 2600 7450
Connection ~ 2600 7450
Wire Wire Line
	2600 7450 2600 7350
Wire Wire Line
	2800 7550 2600 7550
Connection ~ 2600 7550
Wire Wire Line
	2600 7550 2600 7450
Wire Wire Line
	2800 7650 2600 7650
Connection ~ 2600 7650
Wire Wire Line
	2600 7650 2600 7550
Wire Wire Line
	2800 7750 2600 7750
Connection ~ 2600 7750
Wire Wire Line
	2600 7750 2600 7650
Wire Wire Line
	2800 7850 2600 7850
Connection ~ 2600 7850
Wire Wire Line
	2600 7850 2600 7750
Wire Wire Line
	2800 8850 2600 8850
Wire Wire Line
	2600 8850 2600 8750
Wire Wire Line
	2800 8050 2600 8050
Connection ~ 2600 8050
Wire Wire Line
	2600 8050 2600 7950
Wire Wire Line
	2800 8150 2600 8150
Connection ~ 2600 8150
Wire Wire Line
	2600 8150 2600 8050
Wire Wire Line
	2800 8250 2600 8250
Connection ~ 2600 8250
Wire Wire Line
	2600 8250 2600 8150
Wire Wire Line
	2800 8350 2600 8350
Connection ~ 2600 8350
Wire Wire Line
	2600 8350 2600 8250
Wire Wire Line
	2800 8450 2600 8450
Connection ~ 2600 8450
Wire Wire Line
	2600 8450 2600 8350
Wire Wire Line
	2800 8550 2600 8550
Connection ~ 2600 8550
Wire Wire Line
	2600 8550 2600 8450
Wire Wire Line
	2800 8650 2600 8650
Connection ~ 2600 8650
Wire Wire Line
	2600 8650 2600 8550
Wire Wire Line
	2800 8750 2600 8750
Connection ~ 2600 8750
Wire Wire Line
	2600 8750 2600 8650
Connection ~ 2600 7950
$Comp
L power:+3.3V #PWR0111
U 1 1 6281B037
P 2600 6750
F 0 "#PWR0111" H 2600 6600 50  0001 C CNN
F 1 "+3.3V" H 2615 6923 50  0000 C CNN
F 2 "" H 2600 6750 50  0001 C CNN
F 3 "" H 2600 6750 50  0001 C CNN
	1    2600 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 8850 3900 8850
Wire Wire Line
	3000 8750 3900 8750
Wire Wire Line
	3000 8650 3900 8650
Wire Wire Line
	3000 8550 3900 8550
Wire Wire Line
	3000 8450 3900 8450
Wire Wire Line
	3000 8350 3900 8350
Wire Wire Line
	3000 8250 3900 8250
Wire Wire Line
	3000 8150 3900 8150
Wire Wire Line
	3000 8050 3900 8050
Wire Wire Line
	3000 7950 3900 7950
Wire Wire Line
	3000 7850 3900 7850
Wire Wire Line
	3000 7750 3900 7750
Wire Wire Line
	3000 7650 3900 7650
Wire Wire Line
	3000 7550 3900 7550
Wire Wire Line
	3000 7450 3900 7450
Wire Wire Line
	3000 7350 3900 7350
Wire Wire Line
	3000 7250 3900 7250
Wire Wire Line
	3000 7150 3900 7150
Wire Wire Line
	3000 7050 3900 7050
$Comp
L power:+3.3V #PWR0112
U 1 1 62826AB0
P 7300 6750
F 0 "#PWR0112" H 7300 6600 50  0001 C CNN
F 1 "+3.3V" H 7315 6923 50  0000 C CNN
F 2 "" H 7300 6750 50  0001 C CNN
F 3 "" H 7300 6750 50  0001 C CNN
	1    7300 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 8750 7300 8750
Wire Wire Line
	7300 8750 7300 8650
Connection ~ 7300 8650
Wire Wire Line
	7500 8650 7300 8650
Wire Wire Line
	7300 8650 7300 8550
Connection ~ 7300 8550
Wire Wire Line
	7500 8550 7300 8550
Wire Wire Line
	7300 8550 7300 8450
Connection ~ 7300 8450
Wire Wire Line
	7500 8450 7300 8450
Wire Wire Line
	7300 8450 7300 8350
Connection ~ 7300 8350
Wire Wire Line
	7500 8350 7300 8350
Wire Wire Line
	7300 8350 7300 8250
Connection ~ 7300 8250
Wire Wire Line
	7500 8250 7300 8250
Wire Wire Line
	7300 8250 7300 8150
Connection ~ 7300 8150
Wire Wire Line
	7500 8150 7300 8150
Wire Wire Line
	7300 8150 7300 8050
Connection ~ 7300 8050
Wire Wire Line
	7500 8050 7300 8050
Connection ~ 7300 8750
Wire Wire Line
	7300 8850 7300 8750
Wire Wire Line
	7500 8850 7300 8850
Connection ~ 7300 7850
Wire Wire Line
	7500 7850 7300 7850
Wire Wire Line
	7300 7850 7300 7750
Connection ~ 7300 7750
Wire Wire Line
	7500 7750 7300 7750
Wire Wire Line
	7300 7750 7300 7650
Connection ~ 7300 7650
Wire Wire Line
	7500 7650 7300 7650
Wire Wire Line
	7300 7650 7300 7550
Connection ~ 7300 7550
Wire Wire Line
	7500 7550 7300 7550
Wire Wire Line
	7300 7550 7300 7450
Connection ~ 7300 7450
Wire Wire Line
	7500 7450 7300 7450
Wire Wire Line
	7300 7450 7300 7350
Connection ~ 7300 7350
Wire Wire Line
	7500 7350 7300 7350
Wire Wire Line
	7300 7350 7300 7250
Connection ~ 7300 7250
Wire Wire Line
	7500 7250 7300 7250
Wire Wire Line
	7300 7250 7300 7150
Connection ~ 7300 7150
Wire Wire Line
	7500 7150 7300 7150
Wire Wire Line
	7300 7150 7300 7050
Connection ~ 7300 7050
Wire Wire Line
	7500 7050 7300 7050
Wire Wire Line
	7300 8050 7300 7950
Wire Wire Line
	7300 7950 7300 7850
Connection ~ 7300 7950
Wire Wire Line
	7500 7950 7300 7950
Wire Wire Line
	7700 7150 8600 7150
Wire Wire Line
	7700 7250 8600 7250
Wire Wire Line
	7700 7350 8600 7350
Wire Wire Line
	7700 7450 8600 7450
Wire Wire Line
	7700 7550 8600 7550
Wire Wire Line
	7700 7650 8600 7650
Wire Wire Line
	7700 7750 8600 7750
Wire Wire Line
	7700 7050 8600 7050
Wire Wire Line
	7700 7850 8600 7850
Wire Wire Line
	7700 7950 8600 7950
Wire Wire Line
	7700 8050 8600 8050
Wire Wire Line
	7700 8150 8600 8150
Wire Wire Line
	7700 8250 8600 8250
Wire Wire Line
	7700 8350 8600 8350
Wire Wire Line
	7700 8450 8600 8450
Wire Wire Line
	7700 8550 8600 8550
Wire Wire Line
	7700 8650 8600 8650
Wire Wire Line
	7700 8750 8600 8750
Wire Wire Line
	7700 8850 8600 8850
$Comp
L Device:R_Small R2
U 1 1 62BF8CB7
P 2900 7150
F 0 "R2" V 2850 7250 25  0000 C CNN
F 1 "10k" V 2900 7150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7150 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7150 50  0001 C CNN
F 4 "$0.1" H 2900 7150 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7150 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7150 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7150 50  0001 C CNN "Supplier P/N"
	1    2900 7150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 62BF8CC1
P 2900 7250
F 0 "R3" V 2850 7350 25  0000 C CNN
F 1 "10k" V 2900 7250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7250 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7250 50  0001 C CNN
F 4 "$0.1" H 2900 7250 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7250 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7250 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7250 50  0001 C CNN "Supplier P/N"
	1    2900 7250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 62BF8CCB
P 2900 7350
F 0 "R4" V 2850 7450 25  0000 C CNN
F 1 "10k" V 2900 7350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7350 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7350 50  0001 C CNN
F 4 "$0.1" H 2900 7350 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7350 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7350 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7350 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7350 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7350 50  0001 C CNN "Supplier P/N"
	1    2900 7350
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 62BF8CD5
P 2900 7450
F 0 "R5" V 2850 7550 25  0000 C CNN
F 1 "10k" V 2900 7450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7450 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7450 50  0001 C CNN
F 4 "$0.1" H 2900 7450 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7450 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7450 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7450 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7450 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7450 50  0001 C CNN "Supplier P/N"
	1    2900 7450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 62BF8CDF
P 2900 7550
F 0 "R6" V 2850 7650 25  0000 C CNN
F 1 "10k" V 2900 7550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7550 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7550 50  0001 C CNN
F 4 "$0.1" H 2900 7550 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7550 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7550 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7550 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7550 50  0001 C CNN "Supplier P/N"
	1    2900 7550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 62BF8CE9
P 2900 7650
F 0 "R7" V 2850 7750 25  0000 C CNN
F 1 "10k" V 2900 7650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7650 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7650 50  0001 C CNN
F 4 "$0.1" H 2900 7650 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7650 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7650 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7650 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7650 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7650 50  0001 C CNN "Supplier P/N"
	1    2900 7650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R8
U 1 1 62BF8CF3
P 2900 7750
F 0 "R8" V 2850 7850 25  0000 C CNN
F 1 "10k" V 2900 7750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7750 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7750 50  0001 C CNN
F 4 "$0.1" H 2900 7750 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7750 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7750 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7750 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7750 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7750 50  0001 C CNN "Supplier P/N"
	1    2900 7750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 62BF8CFD
P 2900 7850
F 0 "R9" V 2850 7950 25  0000 C CNN
F 1 "10k" V 2900 7850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7850 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7850 50  0001 C CNN
F 4 "$0.1" H 2900 7850 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7850 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7850 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7850 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7850 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7850 50  0001 C CNN "Supplier P/N"
	1    2900 7850
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R10
U 1 1 62BF8D07
P 2900 7950
F 0 "R10" V 2850 8050 25  0000 C CNN
F 1 "10k" V 2900 7950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 7950 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 7950 50  0001 C CNN
F 4 "$0.1" H 2900 7950 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 7950 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 7950 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 7950 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 7950 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 7950 50  0001 C CNN "Supplier P/N"
	1    2900 7950
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 62BF8D11
P 2900 8050
F 0 "R11" V 2850 8150 25  0000 C CNN
F 1 "10k" V 2900 8050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8050 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8050 50  0001 C CNN
F 4 "$0.1" H 2900 8050 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8050 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8050 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8050 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8050 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8050 50  0001 C CNN "Supplier P/N"
	1    2900 8050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 62BF8D1B
P 2900 8150
F 0 "R12" V 2850 8250 25  0000 C CNN
F 1 "10k" V 2900 8150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8150 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8150 50  0001 C CNN
F 4 "$0.1" H 2900 8150 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8150 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8150 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8150 50  0001 C CNN "Supplier P/N"
	1    2900 8150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 62BF8D25
P 2900 8250
F 0 "R13" V 2850 8350 25  0000 C CNN
F 1 "10k" V 2900 8250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8250 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8250 50  0001 C CNN
F 4 "$0.1" H 2900 8250 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8250 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8250 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8250 50  0001 C CNN "Supplier P/N"
	1    2900 8250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R14
U 1 1 62BF8D2F
P 2900 8350
F 0 "R14" V 2850 8450 25  0000 C CNN
F 1 "10k" V 2900 8350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8350 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8350 50  0001 C CNN
F 4 "$0.1" H 2900 8350 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8350 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8350 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8350 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8350 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8350 50  0001 C CNN "Supplier P/N"
	1    2900 8350
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 62BF8D39
P 2900 8450
F 0 "R15" V 2850 8550 25  0000 C CNN
F 1 "10k" V 2900 8450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8450 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8450 50  0001 C CNN
F 4 "$0.1" H 2900 8450 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8450 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8450 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8450 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8450 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8450 50  0001 C CNN "Supplier P/N"
	1    2900 8450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R16
U 1 1 62BF8D43
P 2900 8550
F 0 "R16" V 2850 8650 25  0000 C CNN
F 1 "10k" V 2900 8550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8550 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8550 50  0001 C CNN
F 4 "$0.1" H 2900 8550 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8550 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8550 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8550 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8550 50  0001 C CNN "Supplier P/N"
	1    2900 8550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 62BF8D4D
P 2900 8650
F 0 "R17" V 2850 8750 25  0000 C CNN
F 1 "10k" V 2900 8650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8650 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8650 50  0001 C CNN
F 4 "$0.1" H 2900 8650 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8650 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8650 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8650 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8650 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8650 50  0001 C CNN "Supplier P/N"
	1    2900 8650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R18
U 1 1 62BF8D57
P 2900 8750
F 0 "R18" V 2850 8850 25  0000 C CNN
F 1 "10k" V 2900 8750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8750 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8750 50  0001 C CNN
F 4 "$0.1" H 2900 8750 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8750 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8750 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8750 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8750 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8750 50  0001 C CNN "Supplier P/N"
	1    2900 8750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 62BF8D61
P 2900 8850
F 0 "R19" V 2850 8950 25  0000 C CNN
F 1 "10k" V 2900 8850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8850 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8850 50  0001 C CNN
F 4 "$0.1" H 2900 8850 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8850 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8850 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8850 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8850 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8850 50  0001 C CNN "Supplier P/N"
	1    2900 8850
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R21
U 1 1 62C50E5F
P 7600 7050
F 0 "R21" V 7550 7150 25  0000 C CNN
F 1 "10k" V 7600 7050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7050 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7050 50  0001 C CNN
F 4 "$0.1" H 7600 7050 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7050 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7050 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7050 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7050 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7050 50  0001 C CNN "Supplier P/N"
	1    7600 7050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R22
U 1 1 62C50E69
P 7600 7150
F 0 "R22" V 7550 7250 25  0000 C CNN
F 1 "10k" V 7600 7150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7150 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7150 50  0001 C CNN
F 4 "$0.1" H 7600 7150 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7150 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7150 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7150 50  0001 C CNN "Supplier P/N"
	1    7600 7150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R23
U 1 1 62C50E73
P 7600 7250
F 0 "R23" V 7550 7350 25  0000 C CNN
F 1 "10k" V 7600 7250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7250 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7250 50  0001 C CNN
F 4 "$0.1" H 7600 7250 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7250 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7250 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7250 50  0001 C CNN "Supplier P/N"
	1    7600 7250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R24
U 1 1 62C50E7D
P 7600 7350
F 0 "R24" V 7550 7450 25  0000 C CNN
F 1 "10k" V 7600 7350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7350 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7350 50  0001 C CNN
F 4 "$0.1" H 7600 7350 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7350 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7350 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7350 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7350 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7350 50  0001 C CNN "Supplier P/N"
	1    7600 7350
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R25
U 1 1 62C50E87
P 7600 7450
F 0 "R25" V 7550 7550 25  0000 C CNN
F 1 "10k" V 7600 7450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7450 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7450 50  0001 C CNN
F 4 "$0.1" H 7600 7450 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7450 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7450 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7450 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7450 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7450 50  0001 C CNN "Supplier P/N"
	1    7600 7450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R26
U 1 1 62C50E91
P 7600 7550
F 0 "R26" V 7550 7650 25  0000 C CNN
F 1 "10k" V 7600 7550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7550 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7550 50  0001 C CNN
F 4 "$0.1" H 7600 7550 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7550 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7550 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7550 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7550 50  0001 C CNN "Supplier P/N"
	1    7600 7550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R27
U 1 1 62C50E9B
P 7600 7650
F 0 "R27" V 7550 7750 25  0000 C CNN
F 1 "10k" V 7600 7650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7650 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7650 50  0001 C CNN
F 4 "$0.1" H 7600 7650 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7650 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7650 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7650 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7650 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7650 50  0001 C CNN "Supplier P/N"
	1    7600 7650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R28
U 1 1 62C50EA5
P 7600 7750
F 0 "R28" V 7550 7850 25  0000 C CNN
F 1 "10k" V 7600 7750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7750 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7750 50  0001 C CNN
F 4 "$0.1" H 7600 7750 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7750 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7750 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7750 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7750 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7750 50  0001 C CNN "Supplier P/N"
	1    7600 7750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R29
U 1 1 62C50EAF
P 7600 7850
F 0 "R29" V 7550 7950 25  0000 C CNN
F 1 "10k" V 7600 7850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7850 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7850 50  0001 C CNN
F 4 "$0.1" H 7600 7850 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7850 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7850 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7850 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7850 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7850 50  0001 C CNN "Supplier P/N"
	1    7600 7850
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R30
U 1 1 62C50EB9
P 7600 7950
F 0 "R30" V 7550 8050 25  0000 C CNN
F 1 "10k" V 7600 7950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 7950 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 7950 50  0001 C CNN
F 4 "$0.1" H 7600 7950 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 7950 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 7950 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 7950 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 7950 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 7950 50  0001 C CNN "Supplier P/N"
	1    7600 7950
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R31
U 1 1 62C50EC3
P 7600 8050
F 0 "R31" V 7550 8150 25  0000 C CNN
F 1 "10k" V 7600 8050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8050 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8050 50  0001 C CNN
F 4 "$0.1" H 7600 8050 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8050 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8050 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8050 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8050 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8050 50  0001 C CNN "Supplier P/N"
	1    7600 8050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R32
U 1 1 62C50ECD
P 7600 8150
F 0 "R32" V 7550 8250 25  0000 C CNN
F 1 "10k" V 7600 8150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8150 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8150 50  0001 C CNN
F 4 "$0.1" H 7600 8150 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8150 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8150 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8150 50  0001 C CNN "Supplier P/N"
	1    7600 8150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R33
U 1 1 62C50ED7
P 7600 8250
F 0 "R33" V 7550 8350 25  0000 C CNN
F 1 "10k" V 7600 8250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8250 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8250 50  0001 C CNN
F 4 "$0.1" H 7600 8250 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8250 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8250 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8250 50  0001 C CNN "Supplier P/N"
	1    7600 8250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R34
U 1 1 62C50EE1
P 7600 8350
F 0 "R34" V 7550 8450 25  0000 C CNN
F 1 "10k" V 7600 8350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8350 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8350 50  0001 C CNN
F 4 "$0.1" H 7600 8350 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8350 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8350 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8350 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8350 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8350 50  0001 C CNN "Supplier P/N"
	1    7600 8350
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R35
U 1 1 62C50EEB
P 7600 8450
F 0 "R35" V 7550 8550 25  0000 C CNN
F 1 "10k" V 7600 8450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8450 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8450 50  0001 C CNN
F 4 "$0.1" H 7600 8450 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8450 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8450 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8450 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8450 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8450 50  0001 C CNN "Supplier P/N"
	1    7600 8450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R36
U 1 1 62C50EF5
P 7600 8550
F 0 "R36" V 7550 8650 25  0000 C CNN
F 1 "10k" V 7600 8550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8550 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8550 50  0001 C CNN
F 4 "$0.1" H 7600 8550 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8550 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8550 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8550 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8550 50  0001 C CNN "Supplier P/N"
	1    7600 8550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R37
U 1 1 62C50EFF
P 7600 8650
F 0 "R37" V 7550 8750 25  0000 C CNN
F 1 "10k" V 7600 8650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8650 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8650 50  0001 C CNN
F 4 "$0.1" H 7600 8650 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8650 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8650 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8650 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8650 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8650 50  0001 C CNN "Supplier P/N"
	1    7600 8650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R38
U 1 1 62C50F09
P 7600 8750
F 0 "R38" V 7550 8850 25  0000 C CNN
F 1 "10k" V 7600 8750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8750 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8750 50  0001 C CNN
F 4 "$0.1" H 7600 8750 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8750 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8750 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8750 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8750 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8750 50  0001 C CNN "Supplier P/N"
	1    7600 8750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R20
U 1 1 62C50F13
P 2900 8950
F 0 "R20" V 2850 9050 25  0000 C CNN
F 1 "10k" V 2900 8950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 8950 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 2900 8950 50  0001 C CNN
F 4 "$0.1" H 2900 8950 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 2900 8950 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2900 8950 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2900 8950 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 2900 8950 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 2900 8950 50  0001 C CNN "Supplier P/N"
	1    2900 8950
	0    1    1    0   
$EndComp
$Comp
L Command_Module_RevB:ERM8-013-01-X-D-RA-DS-L-X-XX J1
U 1 1 61F8380E
P 2500 3200
F 0 "J1" H 2500 4067 50  0000 C CNN
F 1 "ERM8-013-01-X-D-RA-DS-L-X-XX" H 2500 3976 50  0000 C CNN
F 2 "SAMTEC_ERM8-013-01-X-D-RA-DS-L-X-XX" H 3150 2200 50  0001 L BNN
F 3 "https://suddendocs.samtec.com/productspecs/erm8-erf8.pdf" H 2500 3200 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 3150 2100 50  0001 L BNN "STANDARD"
F 5 "6.25mm" H 3500 2000 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "" H 3150 2000 50  0001 L BNN "MANUFACTURER"
F 7 "J" H 2500 3200 50  0001 L BNN "PARTREV"
F 8 "$8.89" H 2500 3200 50  0001 C CNN "Item Cost"
F 9 "ERM8-013-01-L-D-RA-DS" H 2500 3200 50  0001 C CNN "MFN"
F 10 "Samtec Inc." H 2500 3200 50  0001 C CNN "Manufacturer"
F 11 "Digi-Key" H 2500 3200 50  0001 C CNN "Supplier"
F 12 "SAM13696DKR-ND" H 2500 3200 50  0001 C CNN "Supplier P/N"
F 13 "https://www.digikey.com/en/products/detail/samtec-inc/ERM8-013-01-L-D-RA-K-TR/6694644?s=N4IgTCBcDa4AxwLQFEBKBZAHHAjAZlwBkARVAQQGkAVVEAXQF8g" H 2500 3200 50  0001 C CNN "Supplier Link"
	1    2500 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2600 1700 2600
Wire Wire Line
	1700 2900 1800 2900
Connection ~ 1700 2900
Wire Wire Line
	1700 2900 1700 3200
Wire Wire Line
	1800 3200 1700 3200
Connection ~ 1700 3200
Wire Wire Line
	1800 3500 1700 3500
Connection ~ 1700 3500
Wire Wire Line
	1700 3500 1700 3800
Wire Wire Line
	1800 3800 1700 3800
Connection ~ 1700 3800
$Comp
L power:GND #PWR01
U 1 1 6352803C
P 1700 4250
F 0 "#PWR01" H 1700 4000 50  0001 C CNN
F 1 "GND" H 1800 4250 50  0000 C CNN
F 2 "" H 1700 4250 50  0001 C CNN
F 3 "" H 1700 4250 50  0001 C CNN
	1    1700 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2600 3300 2600
Wire Wire Line
	3300 2600 3300 2900
Wire Wire Line
	3300 2900 3200 2900
Connection ~ 3300 2900
Wire Wire Line
	3300 2900 3300 3200
Wire Wire Line
	3200 3200 3300 3200
Connection ~ 3300 3200
Wire Wire Line
	3300 3200 3300 3500
Wire Wire Line
	3200 3500 3300 3500
Connection ~ 3300 3500
Wire Wire Line
	3300 3500 3300 3800
Wire Wire Line
	3200 3800 3300 3800
Connection ~ 3300 3800
$Comp
L power:GND #PWR02
U 1 1 63556705
P 3300 4200
F 0 "#PWR02" H 3300 3950 50  0001 C CNN
F 1 "GND" H 3400 4200 50  0000 C CNN
F 2 "" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 2700 1600 3000
Wire Wire Line
	1600 3000 1800 3000
Wire Wire Line
	1800 2800 1550 2800
Wire Wire Line
	1550 2800 1550 3100
Wire Wire Line
	1550 3100 1800 3100
Wire Wire Line
	3200 2700 3400 2700
Wire Wire Line
	3400 2700 3400 3000
Wire Wire Line
	3400 3000 3200 3000
Wire Wire Line
	3200 2800 3450 2800
Wire Wire Line
	3450 2800 3450 3100
Wire Wire Line
	3450 3100 3200 3100
Wire Wire Line
	1800 3700 1650 3700
Wire Wire Line
	1650 3700 1650 3950
Wire Wire Line
	1650 3950 2300 3950
Wire Wire Line
	3350 3950 3350 3700
Wire Wire Line
	3350 3700 3200 3700
Wire Wire Line
	1800 3600 1600 3600
Wire Wire Line
	3400 3600 3200 3600
Wire Wire Line
	1700 3800 1700 4250
Wire Wire Line
	3300 3800 3300 4200
Wire Wire Line
	5300 4300 6000 4300
Wire Wire Line
	7000 3300 7700 3300
Wire Wire Line
	5300 4000 6000 4000
Wire Wire Line
	7000 4200 7700 4200
Wire Wire Line
	7000 4100 7700 4100
Wire Wire Line
	7000 4000 7700 4000
Wire Wire Line
	7000 3900 7700 3900
Wire Wire Line
	7000 3800 7700 3800
Wire Wire Line
	7000 3700 7700 3700
Wire Wire Line
	7000 3600 7700 3600
Wire Wire Line
	7000 3500 7700 3500
Wire Wire Line
	5300 3200 6000 3200
Wire Wire Line
	5300 3900 6000 3900
Wire Wire Line
	5300 3800 6000 3800
Wire Wire Line
	5300 3700 6000 3700
Wire Wire Line
	5300 3600 6000 3600
Wire Wire Line
	5300 3500 6000 3500
Wire Wire Line
	5300 3400 6000 3400
Wire Wire Line
	5300 3300 6000 3300
$Comp
L power:GND #PWR0104
U 1 1 6221A471
P 5950 4400
F 0 "#PWR0104" H 5950 4150 50  0001 C CNN
F 1 "GND" H 6050 4400 50  0000 C CNN
F 2 "" H 5950 4400 50  0001 C CNN
F 3 "" H 5950 4400 50  0001 C CNN
	1    5950 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 622135D8
P 7050 4400
F 0 "#PWR0103" H 7050 4150 50  0001 C CNN
F 1 "GND" H 7150 4400 50  0000 C CNN
F 2 "" H 7050 4400 50  0001 C CNN
F 3 "" H 7050 4400 50  0001 C CNN
	1    7050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4200 5950 4400
Connection ~ 5950 4200
Wire Wire Line
	6000 4200 5950 4200
Wire Wire Line
	5950 4100 5950 4200
Wire Wire Line
	6000 4100 5950 4100
Wire Wire Line
	7050 3400 7050 4400
Connection ~ 7050 3400
Wire Wire Line
	7000 3400 7050 3400
Wire Wire Line
	7050 3200 7050 3400
Wire Wire Line
	7000 3200 7050 3200
Text Label 7700 4200 2    50   ~ 0
J2_MON_RX
Text Label 7700 4100 2    50   ~ 0
J2_SENSE_SCL
Text Label 7700 4000 2    50   ~ 0
J2_RX_ZYNQ
Text Label 7700 3900 2    50   ~ 0
J2_RX_IPMC
Text Label 7700 3800 2    50   ~ 0
J2_TDI
Text Label 7700 3700 2    50   ~ 0
J2_TCK
Text Label 7700 3600 2    50   ~ 0
J2_EN
Text Label 7700 3500 2    50   ~ 0
J2_CPLD_GPIO1
Text Label 7700 3300 2    50   ~ 0
J2_CPLD_GPIO0
Text Label 5300 4300 0    50   ~ 0
J2_FPGA_GPIO2
Text Label 5300 4000 0    50   ~ 0
J2_FPGA_GPIO1
Text Label 5300 3900 0    50   ~ 0
J2_FPGA_GPIO0
Text Label 5300 3800 0    50   ~ 0
J2_PWR_EN
Text Label 5300 3700 0    50   ~ 0
J2_SENSE_SDA
Text Label 5300 3600 0    50   ~ 0
J2_TX_ZYNQ
Text Label 5300 3500 0    50   ~ 0
J2_TX_IPMC
Text Label 5300 3200 0    50   ~ 0
J2_PWR_GOOD
$Comp
L power:+12V #PWR0102
U 1 1 621DABF6
P 5800 2050
F 0 "#PWR0102" H 5800 1900 50  0001 C CNN
F 1 "+12V" H 5650 2100 50  0000 C CNN
F 2 "" H 5800 2050 50  0001 C CNN
F 3 "" H 5800 2050 50  0001 C CNN
	1    5800 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2900 5800 2800
Connection ~ 5800 2900
Wire Wire Line
	6000 2900 5800 2900
Wire Wire Line
	5800 2800 5800 2700
Connection ~ 5800 2800
Wire Wire Line
	6000 2800 5800 2800
Wire Wire Line
	5800 2700 5800 2600
Connection ~ 5800 2700
Wire Wire Line
	6000 2700 5800 2700
Wire Wire Line
	5800 2600 5800 2500
Connection ~ 5800 2600
Wire Wire Line
	6000 2600 5800 2600
Wire Wire Line
	5800 2500 5800 2400
Connection ~ 5800 2500
Wire Wire Line
	6000 2500 5800 2500
Wire Wire Line
	5800 2400 5800 2300
Connection ~ 5800 2400
Wire Wire Line
	6000 2400 5800 2400
Wire Wire Line
	5800 2300 5800 2200
Connection ~ 5800 2300
Wire Wire Line
	6000 2300 5800 2300
Wire Wire Line
	5800 2200 5800 2100
Connection ~ 5800 2200
Wire Wire Line
	6000 2200 5800 2200
Wire Wire Line
	5800 2100 5800 2050
Connection ~ 5800 2100
Wire Wire Line
	6000 2100 5800 2100
Wire Wire Line
	5800 3000 5800 2900
Wire Wire Line
	6000 3000 5800 3000
$Comp
L power:GND #PWR0101
U 1 1 621CBF41
P 7200 3050
F 0 "#PWR0101" H 7200 2800 50  0001 C CNN
F 1 "GND" H 7300 3050 50  0000 C CNN
F 2 "" H 7200 3050 50  0001 C CNN
F 3 "" H 7200 3050 50  0001 C CNN
	1    7200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2200 7200 2300
Connection ~ 7200 2200
Wire Wire Line
	7000 2200 7200 2200
Wire Wire Line
	7200 2300 7200 2400
Connection ~ 7200 2300
Wire Wire Line
	7000 2300 7200 2300
Wire Wire Line
	7200 2400 7200 2500
Connection ~ 7200 2400
Wire Wire Line
	7000 2400 7200 2400
Wire Wire Line
	7200 2500 7200 2600
Connection ~ 7200 2500
Wire Wire Line
	7000 2500 7200 2500
Wire Wire Line
	7200 2600 7200 2700
Connection ~ 7200 2600
Wire Wire Line
	7000 2600 7200 2600
Wire Wire Line
	7200 2700 7200 2800
Connection ~ 7200 2700
Wire Wire Line
	7000 2700 7200 2700
Wire Wire Line
	7200 2800 7200 2900
Connection ~ 7200 2800
Wire Wire Line
	7000 2800 7200 2800
Wire Wire Line
	7200 2900 7200 3000
Connection ~ 7200 2900
Wire Wire Line
	7000 2900 7200 2900
Wire Wire Line
	7200 3000 7200 3050
Connection ~ 7200 3000
Wire Wire Line
	7000 3000 7200 3000
Wire Wire Line
	7200 2100 7200 2200
Wire Wire Line
	7000 2100 7200 2100
$Comp
L Command_Module_RevB:ET60T-D02-3-08-000-X-R1-S J2
U 1 1 61F85580
P 6500 3150
F 0 "J2" H 6500 4500 50  0000 C CNN
F 1 "ET60T-D02-3-08-000-X-R1-S" H 6500 4400 50  0000 C CNN
F 2 "Command_Module:SAMTEC_ET60T-D02-3-08-000-X-R1-S_RevB" H 7050 2100 50  0001 L BNN
F 3 "https://suddendocs.samtec.com/productspecs/et60s-et60t.pdf" H 6500 3150 50  0001 L BNN
F 4 "10 mm" H 7350 1900 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 5 "Manufacturer Recommendations" H 7050 2000 50  0001 L BNN "STANDARD"
F 6 "J" H 6500 3150 50  0001 L BNN "PARTREV"
F 7 "" H 7050 1900 50  0001 L BNN "MANUFACTURER"
F 8 "$9.52" H 6500 3150 50  0001 C CNN "Item Cost"
F 9 "ET60T-D02-3-08-000-L-R1-S" H 6500 3150 50  0001 C CNN "MFN"
F 10 "Samtec Inc." H 6500 3150 50  0001 C CNN "Manufacturer"
F 11 "Digi-Key" H 6500 3150 50  0001 C CNN "Supplier"
F 12 "SAM15977-ND" H 6500 3150 50  0001 C CNN "Supplier P/N"
F 13 "https://www.digikey.com/en/products/detail/samtec-inc/ET60T-D02-3-08-000-L-R1-S/10064502?s=N4IgTCBcDaIMoEECyBGArATgOxYLQDkAREAXQF8g" H 6500 3150 50  0001 C CNN "Supplier Link"
	1    6500 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 2600 9700 2600
Wire Wire Line
	9700 2600 9700 2900
Wire Wire Line
	9700 2900 9800 2900
Connection ~ 9700 2900
Wire Wire Line
	9700 2900 9700 3200
Wire Wire Line
	9800 3200 9700 3200
Connection ~ 9700 3200
Wire Wire Line
	9700 3200 9700 3500
Wire Wire Line
	9800 3500 9700 3500
Connection ~ 9700 3500
Wire Wire Line
	9700 3500 9700 3800
Wire Wire Line
	9800 3800 9700 3800
Connection ~ 9700 3800
$Comp
L power:GND #PWR03
U 1 1 63A6486A
P 9700 4250
F 0 "#PWR03" H 9700 4000 50  0001 C CNN
F 1 "GND" H 9800 4250 50  0000 C CNN
F 2 "" H 9700 4250 50  0001 C CNN
F 3 "" H 9700 4250 50  0001 C CNN
	1    9700 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11200 2600 11300 2600
Wire Wire Line
	11300 2600 11300 2900
Wire Wire Line
	11300 2900 11200 2900
Connection ~ 11300 2900
Wire Wire Line
	11300 2900 11300 3200
Wire Wire Line
	11200 3200 11300 3200
Connection ~ 11300 3200
Wire Wire Line
	11300 3200 11300 3500
Wire Wire Line
	11200 3500 11300 3500
Connection ~ 11300 3500
Wire Wire Line
	11300 3500 11300 3800
Wire Wire Line
	11200 3800 11300 3800
Connection ~ 11300 3800
$Comp
L power:GND #PWR04
U 1 1 63A64881
P 11300 4200
F 0 "#PWR04" H 11300 3950 50  0001 C CNN
F 1 "GND" H 11400 4200 50  0000 C CNN
F 2 "" H 11300 4200 50  0001 C CNN
F 3 "" H 11300 4200 50  0001 C CNN
	1    11300 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9800 2700 9600 2700
Wire Wire Line
	9600 2700 9600 3000
Wire Wire Line
	9600 3000 9800 3000
Wire Wire Line
	9800 2800 9550 2800
Wire Wire Line
	9550 2800 9550 3100
Wire Wire Line
	9550 3100 9800 3100
Wire Wire Line
	11200 2700 11400 2700
Wire Wire Line
	11400 2700 11400 3000
Wire Wire Line
	11400 3000 11200 3000
Wire Wire Line
	11200 2800 11450 2800
Wire Wire Line
	11450 2800 11450 3100
Wire Wire Line
	11450 3100 11200 3100
Wire Wire Line
	9800 3700 9650 3700
Wire Wire Line
	9650 3700 9650 3950
Wire Wire Line
	9650 3950 10300 3950
Wire Wire Line
	11350 3950 11350 3700
Wire Wire Line
	11350 3700 11200 3700
Wire Wire Line
	9800 3600 9600 3600
Wire Wire Line
	11400 3600 11200 3600
Wire Wire Line
	9700 3800 9700 4250
Wire Wire Line
	11300 3800 11300 4200
Text Label 1200 3300 0    50   ~ 0
J1_HQ_CLK_P
Text Label 1200 3400 0    50   ~ 0
J1_HQ_CLK_N
Text Label 3900 3300 2    50   ~ 0
J1_LHC_CLK_P
Text Label 3900 3400 2    50   ~ 0
J1_LHC_CLK_N
Text Label 9200 3300 0    50   ~ 0
J3_HQ_CLK_P
Text Label 9200 3400 0    50   ~ 0
J3_HQ_CLK_N
Text Label 11900 3300 2    50   ~ 0
J3_LHC_CLK_P
Text Label 11900 3400 2    50   ~ 0
J3_LHC_CLK_N
Wire Wire Line
	1200 3400 1800 3400
Wire Wire Line
	1700 3200 1700 3500
Wire Wire Line
	1200 3300 1800 3300
Wire Wire Line
	3200 3300 3900 3300
Wire Wire Line
	3200 3400 3900 3400
Wire Wire Line
	9200 3300 9800 3300
Wire Wire Line
	9200 3400 9800 3400
Wire Wire Line
	11200 3300 11900 3300
Wire Wire Line
	11200 3400 11900 3400
$Comp
L Connector_Generic:Conn_01x03 J10
U 1 1 63A648A3
P 12100 3300
F 0 "J10" H 12180 3292 50  0000 L CNN
F 1 "Conn_01x03" H 12180 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 12100 3300 50  0001 C CNN
F 3 "https://app.adam-tech.com/products/download/data_sheet/201605/ph1-xx-ua-data-sheet.pdf" H 12100 3300 50  0001 C CNN
F 4 "$0.1" H 12100 3300 50  0001 C CNN "Item Cost"
F 5 "PH1-03-UA" H 12100 3300 50  0001 C CNN "MFN"
F 6 "Adam Tech" H 12100 3300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12100 3300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/adam-tech/PH1-03-UA/9830289" H 12100 3300 50  0001 C CNN "Supplier Link"
F 9 "2057-PH1-03-UA-ND" H 12100 3300 50  0001 C CNN "Supplier P/N"
	1    12100 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J9
U 1 1 63A648AF
P 9000 3300
F 0 "J9" H 9080 3292 50  0000 L CNN
F 1 "Conn_01x03" H 9080 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9000 3300 50  0001 C CNN
F 3 "https://app.adam-tech.com/products/download/data_sheet/201605/ph1-xx-ua-data-sheet.pdf" H 9000 3300 50  0001 C CNN
F 4 "$0.1" H 9000 3300 50  0001 C CNN "Item Cost"
F 5 "PH1-03-UA" H 9000 3300 50  0001 C CNN "MFN"
F 6 "Adam Tech" H 9000 3300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9000 3300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/adam-tech/PH1-03-UA/9830289" H 9000 3300 50  0001 C CNN "Supplier Link"
F 9 "2057-PH1-03-UA-ND" H 9000 3300 50  0001 C CNN "Supplier P/N"
	1    9000 3300
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 6376C567
P 1000 3300
F 0 "J7" H 1080 3292 50  0000 L CNN
F 1 "Conn_01x03" H 1080 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1000 3300 50  0001 C CNN
F 3 "https://app.adam-tech.com/products/download/data_sheet/201605/ph1-xx-ua-data-sheet.pdf" H 1000 3300 50  0001 C CNN
F 4 "$0.1" H 1000 3300 50  0001 C CNN "Item Cost"
F 5 "PH1-03-UA" H 1000 3300 50  0001 C CNN "MFN"
F 6 "Adam Tech" H 1000 3300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 1000 3300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/adam-tech/PH1-03-UA/9830289" H 1000 3300 50  0001 C CNN "Supplier Link"
F 9 "2057-PH1-03-UA-ND" H 1000 3300 50  0001 C CNN "Supplier P/N"
	1    1000 3300
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J8
U 1 1 63700B27
P 4100 3300
F 0 "J8" H 4180 3292 50  0000 L CNN
F 1 "Conn_01x03" H 4180 3201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4100 3300 50  0001 C CNN
F 3 "https://app.adam-tech.com/products/download/data_sheet/201605/ph1-xx-ua-data-sheet.pdf" H 4100 3300 50  0001 C CNN
F 4 "$0.1" H 4100 3300 50  0001 C CNN "Item Cost"
F 5 "PH1-03-UA" H 4100 3300 50  0001 C CNN "MFN"
F 6 "Adam Tech" H 4100 3300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 4100 3300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/adam-tech/PH1-03-UA/9830289" H 4100 3300 50  0001 C CNN "Supplier Link"
F 9 "2057-PH1-03-UA-ND" H 4100 3300 50  0001 C CNN "Supplier P/N"
	1    4100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 3200 1700 3200
Wire Wire Line
	3900 3200 3300 3200
Wire Wire Line
	9200 3200 9700 3200
Wire Wire Line
	11900 3200 11300 3200
$Comp
L Device:R_Small R41
U 1 1 6287151F
P 2500 4000
F 0 "R41" V 2450 4150 50  0000 C CNN
F 1 "100" V 2500 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2500 4000 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 2500 4000 50  0001 C CNN
F 4 "$0.12" H 2500 4000 50  0001 C CNN "Item Cost"
F 5 "ERA-6AED101V" H 2500 4000 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 2500 4000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 2500 4000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERA-6AED101V/9373572" H 2500 4000 50  0001 C CNN "Supplier Link"
F 9 "P123792DKR-ND" H 2500 4000 50  0001 C CNN "Supplier P/N"
	1    2500 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 4050 2700 4050
Wire Wire Line
	1600 3600 1600 4050
Wire Wire Line
	3400 3600 3400 4050
Wire Wire Line
	2300 3950 2300 4000
Wire Wire Line
	2300 4000 2400 4000
Connection ~ 2300 3950
Wire Wire Line
	2300 3950 3350 3950
Wire Wire Line
	2600 4000 2700 4000
Wire Wire Line
	2700 4000 2700 4050
Connection ~ 2700 4050
Wire Wire Line
	2700 4050 3400 4050
$Comp
L Device:R_Small R42
U 1 1 6292C05D
P 10500 4000
F 0 "R42" V 10450 4150 50  0000 C CNN
F 1 "100" V 10500 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10500 4000 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 10500 4000 50  0001 C CNN
F 4 "$0.12" H 10500 4000 50  0001 C CNN "Item Cost"
F 5 "ERA-6AED101V" H 10500 4000 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 10500 4000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 10500 4000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERA-6AED101V/9373572" H 10500 4000 50  0001 C CNN "Supplier Link"
F 9 "P123792DKR-ND" H 10500 4000 50  0001 C CNN "Supplier P/N"
	1    10500 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4050 10700 4050
Wire Wire Line
	10300 3950 10300 4000
Wire Wire Line
	10300 4000 10400 4000
Connection ~ 10300 3950
Wire Wire Line
	10300 3950 11350 3950
Wire Wire Line
	10600 4000 10700 4000
Wire Wire Line
	10700 4000 10700 4050
Connection ~ 10700 4050
Wire Wire Line
	10700 4050 11400 4050
Wire Wire Line
	9600 3600 9600 4050
Wire Wire Line
	11400 3600 11400 4050
Wire Wire Line
	7000 4300 7700 4300
Text Label 7700 4300 2    50   ~ 0
J2_PS_RST
Wire Wire Line
	15000 4300 15700 4300
Text Label 15700 4300 2    50   ~ 0
J4_PS_RST
Text Label 7900 8950 0    50   ~ 0
J4_PS_RST
Text Label 3200 8950 0    50   ~ 0
J2_PS_RST
Wire Wire Line
	7500 8950 7300 8950
$Comp
L Device:R_Small R40
U 1 1 62B4F0C5
P 7600 8950
F 0 "R40" V 7550 9050 25  0000 C CNN
F 1 "10k" V 7600 8950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8950 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8950 50  0001 C CNN
F 4 "$0.1" H 7600 8950 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8950 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8950 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8950 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8950 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8950 50  0001 C CNN "Supplier P/N"
	1    7600 8950
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 8850 7300 8950
Connection ~ 7300 8850
Wire Wire Line
	7700 8950 8600 8950
Wire Wire Line
	2800 8950 2600 8950
$Comp
L Device:R_Small R39
U 1 1 62C16DD8
P 7600 8850
F 0 "R39" V 7550 8950 25  0000 C CNN
F 1 "10k" V 7600 8850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7600 8850 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ103V" H 7600 8850 50  0001 C CNN
F 4 "$0.1" H 7600 8850 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ103V" H 7600 8850 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 7600 8850 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7600 8850 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ103V/83062" H 7600 8850 50  0001 C CNN "Supplier Link"
F 9 "P10KADKR-ND" H 7600 8850 50  0001 C CNN "Supplier P/N"
	1    7600 8850
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 8850 2600 8950
Connection ~ 2600 8850
Wire Wire Line
	3000 8950 3900 8950
Wire Wire Line
	1800 2700 1600 2700
Wire Wire Line
	1700 2600 1700 2900
Text Notes 3200 2700 0    50   ~ 0
J1_TX2_P
Text Notes 3200 2800 0    50   ~ 0
J1_TX2_N
Text Notes 3200 3000 0    50   ~ 0
J1_RX2_P
Text Notes 3200 3100 0    50   ~ 0
J1_RX2_N
Text Notes 3200 3600 0    50   ~ 0
J1_TTS_P\n
Text Notes 3200 3700 0    50   ~ 0
J1_TTS_N\n
Text Notes 1800 3700 2    50   ~ 0
J1_TTC_N
Text Notes 1800 3600 2    50   ~ 0
J1_TTC_P\n
Text Notes 1800 3100 2    50   ~ 0
J1_RX1_N
Text Notes 1800 3000 2    50   ~ 0
J1_RX1_P\n
Text Notes 1800 2800 2    50   ~ 0
J1_TX1_N
Text Notes 1800 2700 2    50   ~ 0
J1_TX1_P
Text Notes 9800 2700 2    50   ~ 0
J3_TX1_P
Text Notes 9800 2800 2    50   ~ 0
J3_TX1_N
Text Notes 9800 3000 2    50   ~ 0
J3_RX1_P
Text Notes 9800 3100 2    50   ~ 0
J3_RX1_N
Text Notes 9800 3600 2    50   ~ 0
J3_TTC_P
Text Notes 9800 3700 2    50   ~ 0
J3_TTC_N
Text Notes 11200 3700 0    50   ~ 0
J3_TTS_N
Text Notes 11200 3600 0    50   ~ 0
J3_TTS_P\n
Text Notes 11200 3100 0    50   ~ 0
J3_RX2_N
Text Notes 11200 3000 0    50   ~ 0
J3_RX2_P
Text Notes 11200 2800 0    50   ~ 0
J3_TX2_N
Text Notes 11200 2700 0    50   ~ 0
J3_TX2_P
$Comp
L Connector:Screw_Terminal_01x01 J11
U 1 1 62269348
P 4000 5500
F 0 "J11" H 4080 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 4080 5451 50  0000 L CNN
F 2 "Command_Module:7693_ScrewTerminal" H 4000 5500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p66.pdf" H 4000 5500 50  0001 C CNN
F 4 "$0.69" H 4000 5500 50  0001 C CNN "Item Cost"
F 5 "7693" H 4000 5500 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 4000 5500 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/keystone-electronics/7693/151562?s=N4IgTCBcDaIMwDYC0B2BBOOSByAREAugL5A" H 4000 5500 50  0001 C CNN "Supplier Link"
F 8 "36-7693-ND" H 4000 5500 50  0001 C CNN "Supplier P/N"
F 9 "Keystone Electronics" H 4000 5500 50  0001 C CNN "Manufacturer"
	1    4000 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J12
U 1 1 6226C780
P 5500 5500
F 0 "J12" H 5580 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 5580 5451 50  0000 L CNN
F 2 "Command_Module:7693_ScrewTerminal" H 5500 5500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p66.pdf" H 5500 5500 50  0001 C CNN
F 4 "$0.69" H 5500 5500 50  0001 C CNN "Item Cost"
F 5 "7693" H 5500 5500 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 5500 5500 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/keystone-electronics/7693/151562?s=N4IgTCBcDaIMwDYC0B2BBOOSByAREAugL5A" H 5500 5500 50  0001 C CNN "Supplier Link"
F 8 "36-7693-ND" H 5500 5500 50  0001 C CNN "Supplier P/N"
F 9 "Keystone Electronics" H 5500 5500 50  0001 C CNN "Manufacturer"
	1    5500 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J13
U 1 1 622BC040
P 7000 5500
F 0 "J13" H 7080 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 7080 5451 50  0000 L CNN
F 2 "Command_Module:7693_ScrewTerminal" H 7000 5500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p66.pdf" H 7000 5500 50  0001 C CNN
F 4 "$0.69" H 7000 5500 50  0001 C CNN "Item Cost"
F 5 "7693" H 7000 5500 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 7000 5500 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/keystone-electronics/7693/151562?s=N4IgTCBcDaIMwDYC0B2BBOOSByAREAugL5A" H 7000 5500 50  0001 C CNN "Supplier Link"
F 8 "36-7693-ND" H 7000 5500 50  0001 C CNN "Supplier P/N"
F 9 "Keystone Electronics" H 7000 5500 50  0001 C CNN "Manufacturer"
	1    7000 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J14
U 1 1 622BC04A
P 8500 5500
F 0 "J14" H 8580 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 8580 5451 50  0000 L CNN
F 2 "Command_Module:7693_ScrewTerminal" H 8500 5500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p66.pdf" H 8500 5500 50  0001 C CNN
F 4 "$0.69" H 8500 5500 50  0001 C CNN "Item Cost"
F 5 "7693" H 8500 5500 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 8500 5500 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/keystone-electronics/7693/151562?s=N4IgTCBcDaIMwDYC0B2BBOOSByAREAugL5A" H 8500 5500 50  0001 C CNN "Supplier Link"
F 8 "36-7693-ND" H 8500 5500 50  0001 C CNN "Supplier P/N"
F 9 "Keystone Electronics" H 8500 5500 50  0001 C CNN "Manufacturer"
	1    8500 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 62316277
P 3700 5600
F 0 "#PWR0113" H 3700 5350 50  0001 C CNN
F 1 "GND" H 3800 5600 50  0000 C CNN
F 2 "" H 3700 5600 50  0001 C CNN
F 3 "" H 3700 5600 50  0001 C CNN
	1    3700 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5600 3700 5500
Wire Wire Line
	3700 5500 3800 5500
$Comp
L power:GND #PWR0114
U 1 1 623A522A
P 5200 5600
F 0 "#PWR0114" H 5200 5350 50  0001 C CNN
F 1 "GND" H 5300 5600 50  0000 C CNN
F 2 "" H 5200 5600 50  0001 C CNN
F 3 "" H 5200 5600 50  0001 C CNN
	1    5200 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5600 5200 5500
Wire Wire Line
	5200 5500 5300 5500
Wire Wire Line
	6800 5500 6700 5500
Wire Wire Line
	6700 5500 6700 5400
Wire Wire Line
	8300 5500 8200 5500
Wire Wire Line
	8200 5500 8200 5400
$Comp
L power:+12V #PWR0115
U 1 1 6247A88D
P 8200 5400
F 0 "#PWR0115" H 8200 5250 50  0001 C CNN
F 1 "+12V" H 8050 5450 50  0000 C CNN
F 2 "" H 8200 5400 50  0001 C CNN
F 3 "" H 8200 5400 50  0001 C CNN
	1    8200 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0116
U 1 1 624CBDF3
P 6700 5400
F 0 "#PWR0116" H 6700 5250 50  0001 C CNN
F 1 "+12V" H 6550 5450 50  0000 C CNN
F 2 "" H 6700 5400 50  0001 C CNN
F 3 "" H 6700 5400 50  0001 C CNN
	1    6700 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 8450 4500 8550
Wire Wire Line
	4400 8950 4500 8950
Connection ~ 4500 8950
Wire Wire Line
	4500 8950 4500 9000
Wire Wire Line
	4400 8850 4500 8850
Connection ~ 4500 8850
Wire Wire Line
	4500 8850 4500 8950
Wire Wire Line
	4400 8750 4500 8750
Connection ~ 4500 8750
Wire Wire Line
	4500 8750 4500 8850
Wire Wire Line
	4400 8650 4500 8650
Connection ~ 4500 8650
Wire Wire Line
	4500 8650 4500 8750
Wire Wire Line
	4400 8550 4500 8550
Connection ~ 4500 8550
Wire Wire Line
	4500 8550 4500 8650
$Comp
L power:+12V #PWR0117
U 1 1 627606BD
P 3000 6550
F 0 "#PWR0117" H 3000 6400 50  0001 C CNN
F 1 "+12V" H 2850 6600 50  0000 C CNN
F 2 "" H 3000 6550 50  0001 C CNN
F 3 "" H 3000 6550 50  0001 C CNN
	1    3000 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 6550 3900 6550
Wire Wire Line
	3000 6550 3000 6650
Wire Wire Line
	3000 6650 3900 6650
Connection ~ 3000 6550
Wire Wire Line
	2600 6850 3900 6850
Wire Wire Line
	3900 6750 2600 6750
NoConn ~ 3900 6950
$Comp
L Connector_Generic:Conn_02x25_Odd_Even J6
U 1 1 6290883C
P 8800 7750
F 0 "J6" H 8850 9167 50  0000 C CNN
F 1 "Conn_02x25_Odd_Even" H 8850 9076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x25_P2.54mm_Vertical" H 8800 7750 50  0001 C CNN
F 3 "https://drawings-pdf.s3.amazonaws.com/11636.pdf" H 8800 7750 50  0001 C CNN
F 4 "$0.86" H 8800 7750 50  0001 C CNN "Item Cost"
F 5 "PREC025DAAN-RC" H 8800 7750 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 8800 7750 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/PREC025DAAN-RC/2774869?s=N4IgTCBcDaIMpgAwEYwFEDCBaMBWLAcgCIgC6AvkA" H 8800 7750 50  0001 C CNN "Supplier Link"
F 8 "S2012EC-25-ND" H 8800 7750 50  0001 C CNN "Supplier P/N"
F 9 "Sullins Connector Solutions" H 8800 7750 50  0001 C CNN "Manufacturer"
	1    8800 7750
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0118
U 1 1 62908846
P 7700 6550
F 0 "#PWR0118" H 7700 6400 50  0001 C CNN
F 1 "+12V" H 7550 6600 50  0000 C CNN
F 2 "" H 7700 6550 50  0001 C CNN
F 3 "" H 7700 6550 50  0001 C CNN
	1    7700 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 6550 8600 6550
Wire Wire Line
	7700 6550 7700 6650
Wire Wire Line
	7700 6650 8600 6650
Connection ~ 7700 6550
Wire Wire Line
	7300 6850 8600 6850
Wire Wire Line
	8600 6750 7300 6750
Wire Wire Line
	7300 6750 7300 6850
NoConn ~ 8600 6950
Wire Wire Line
	9200 8450 9200 8550
Wire Wire Line
	9100 8550 9200 8550
Connection ~ 9200 8550
Wire Wire Line
	9200 8550 9200 8650
Wire Wire Line
	9100 8650 9200 8650
Connection ~ 9200 8650
Wire Wire Line
	9200 8650 9200 8750
Wire Wire Line
	9100 8750 9200 8750
Connection ~ 9200 8750
Wire Wire Line
	9200 8750 9200 8850
Wire Wire Line
	9100 8850 9200 8850
Connection ~ 9200 8850
Wire Wire Line
	9100 8950 9200 8950
Wire Wire Line
	9200 8850 9200 8950
Connection ~ 9200 8950
Wire Wire Line
	9200 8950 9200 9000
$Comp
L Connector_Generic:Conn_02x25_Odd_Even J5
U 1 1 6245B7AA
P 4100 7750
F 0 "J5" H 4150 9167 50  0000 C CNN
F 1 "Conn_02x25_Odd_Even" H 4150 9076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x25_P2.54mm_Vertical" H 4100 7750 50  0001 C CNN
F 3 "https://drawings-pdf.s3.amazonaws.com/11636.pdf" H 4100 7750 50  0001 C CNN
F 4 "$0.86" H 4100 7750 50  0001 C CNN "Item Cost"
F 5 "PREC025DAAN-RC" H 4100 7750 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 4100 7750 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/PREC025DAAN-RC/2774869?s=N4IgTCBcDaIMpgAwEYwFEDCBaMBWLAcgCIgC6AvkA" H 4100 7750 50  0001 C CNN "Supplier Link"
F 8 "S2012EC-25-ND" H 4100 7750 50  0001 C CNN "Supplier P/N"
F 9 "Sullins Connector Solutions" H 4100 7750 50  0001 C CNN "Manufacturer"
	1    4100 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 6750 2600 6850
Connection ~ 2600 6750
Connection ~ 2600 6850
Wire Wire Line
	2600 6850 2600 7050
Connection ~ 7300 6750
Wire Wire Line
	7300 6850 7300 7050
Connection ~ 7300 6850
$Comp
L Command_Module_RevB:TPSM84203 U1
U 1 1 624BC13B
P 14000 6200
F 0 "U1" H 14000 6525 50  0000 C CNN
F 1 "TPSM84203" H 14000 6434 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 14000 6200 50  0001 C CNN
F 3 "https://www.ti.com/document-viewer/TPSM84203/datasheet/abstract#SLUSCV78355" H 14000 6200 50  0001 C CNN
F 4 "$7.59" H 14000 6200 50  0001 C CNN "Item Cost"
F 5 "TPSM84203EAB" H 14000 6200 50  0001 C CNN "MFN"
F 6 "Mouser Electronics" H 14000 6200 50  0001 C CNN "Supplier"
F 7 "https://www.mouser.com/ProductDetail/595-TPSM84203EAB" H 14000 6200 50  0001 C CNN "Supplier Link"
F 8 "595-TPSM84203EAB" H 14000 6200 50  0001 C CNN "Supplier P/N"
F 9 "Texas Instruments" H 14000 6200 50  0001 C CNN "Manufacturer"
	1    14000 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12800 6100 12800 6050
$Comp
L power:+12V #PWR0119
U 1 1 624C2B86
P 12800 6050
F 0 "#PWR0119" H 12800 5900 50  0001 C CNN
F 1 "+12V" H 12650 6100 50  0000 C CNN
F 2 "" H 12800 6050 50  0001 C CNN
F 3 "" H 12800 6050 50  0001 C CNN
	1    12800 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D1
U 1 1 624C2B96
P 12950 6550
F 0 "D1" V 12996 6482 50  0000 R CNN
F 1 "Green_LED" V 12905 6482 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" V 12950 6550 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/151031VS06000.pdf" V 12950 6550 50  0001 C CNN
F 4 "$0.16" H 12950 6550 50  0001 C CNN "Item Cost"
F 5 "151031VS06000" H 12950 6550 50  0001 C CNN "MFN"
F 6 "Würth Elektronik" H 12950 6550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12950 6550 50  0001 C CNN "Supplier"
F 8 "732-5008-ND" H 12950 6550 50  0001 C CNN "Supplier P/N"
F 9 "https://www.digikey.com/en/products/detail/w%C3%BCrth-elektronik/151031VS06000/4489988?s=N4IgTCBcDaIOwGYwFoCsAGdAOZA5AIiALoC%2BQA" H 12950 6550 50  0001 C CNN "Supplier Link"
	1    12950 6550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 6350 12950 6450
$Comp
L Device:R_Small R43
U 1 1 624C2BA7
P 12950 6250
F 0 "R43" H 12850 6250 50  0000 C CNN
F 1 "5k" V 12950 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 12950 6250 50  0001 C CNN
F 3 "http://www.vishaypg.com/docs/63106/VFCP.pdf" H 12950 6250 50  0001 C CNN
F 4 "$8.53" H 12950 6250 50  0001 C CNN "Item Cost"
F 5 "Y16295K00000T9R" H 12950 6250 50  0001 C CNN "MFN"
F 6 "Vishay Foil Resistors (Division of Vishay Precision Group)" H 12950 6250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12950 6250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/vishay-foil-resistors-division-of-vishay-precision-group/Y16295K00000T9R/4333531" H 12950 6250 50  0001 C CNN "Supplier Link"
F 9 "Y1629-5.0KADKR-ND" H 12950 6250 50  0001 C CNN "Supplier P/N"
	1    12950 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	12950 6650 12950 6700
$Comp
L power:GND #PWR0120
U 1 1 624C2BB2
P 12950 6700
F 0 "#PWR0120" H 12950 6450 50  0001 C CNN
F 1 "GND" H 13050 6700 50  0000 C CNN
F 2 "" H 12950 6700 50  0001 C CNN
F 3 "" H 12950 6700 50  0001 C CNN
	1    12950 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	12800 6100 12950 6100
Wire Wire Line
	12950 6100 12950 6150
Connection ~ 12950 6100
Wire Wire Line
	13450 6100 13450 6200
$Comp
L Device:C_Small C1
U 1 1 624C2BC6
P 13450 6300
F 0 "C1" H 13542 6346 50  0000 L CNN
F 1 "10uF" H 13542 6255 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 13450 6300 50  0001 C CNN
F 3 "http://weblib.samsungsem.com/mlcc/mlcc-ec-data-sheet.do?partNumber=CL31A106KOHNNN" H 13450 6300 50  0001 C CNN
F 4 "$0.22" H 13450 6300 50  0001 C CNN "Item Cost"
F 5 "CL31A106KOHNNNE" H 13450 6300 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 13450 6300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 13450 6300 50  0001 C CNN "Supplier"
F 8 "1276-1137-6-ND" H 13450 6300 50  0001 C CNN "Supplier P/N"
F 9 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL31A106KOHNNNE/3886795" H 13450 6300 50  0001 C CNN "Supplier Link"
	1    13450 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 624C2BD0
P 13450 6550
F 0 "#PWR0121" H 13450 6300 50  0001 C CNN
F 1 "GND" H 13550 6550 50  0000 C CNN
F 2 "" H 13450 6550 50  0001 C CNN
F 3 "" H 13450 6550 50  0001 C CNN
	1    13450 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	13450 6400 13450 6550
Wire Wire Line
	12950 6100 13450 6100
Connection ~ 13450 6100
Wire Wire Line
	13450 6100 13650 6100
$Comp
L power:+3.3V #PWR0122
U 1 1 6252F3F1
P 15150 6050
F 0 "#PWR0122" H 15150 5900 50  0001 C CNN
F 1 "+3.3V" H 15165 6223 50  0000 C CNN
F 2 "" H 15150 6050 50  0001 C CNN
F 3 "" H 15150 6050 50  0001 C CNN
	1    15150 6050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	15150 6100 15150 6050
Wire Wire Line
	14650 6100 14650 6200
$Comp
L Device:C_Small C3
U 1 1 6252F403
P 14650 6300
F 0 "C3" H 14700 6400 50  0000 L CNN
F 1 "47uF" H 14650 6250 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 14650 6300 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM31CE70G476ME15-01.pdf" H 14650 6300 50  0001 C CNN
F 4 "$0.71" H 14650 6300 50  0001 C CNN "Item Cost"
F 5 "GRM31CE70G476ME15K" H 14650 6300 50  0001 C CNN "MFN"
F 6 "Würth Elektronik" H 14650 6300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 14650 6300 50  0001 C CNN "Supplier"
F 8 "490-GRM31CE70G476ME15KDKR-ND" H 14650 6300 50  0001 C CNN "Supplier P/N"
F 9 "https://www.digikey.com/en/products/detail/murata-electronics/GRM31CE70G476ME15K/13904815" H 14650 6300 50  0001 C CNN "Supplier Link"
	1    14650 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 6252F40D
P 14650 6550
F 0 "#PWR0123" H 14650 6300 50  0001 C CNN
F 1 "GND" H 14750 6550 50  0000 C CNN
F 2 "" H 14650 6550 50  0001 C CNN
F 3 "" H 14650 6550 50  0001 C CNN
	1    14650 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	14650 6400 14650 6550
$Comp
L Device:LED_Small D2
U 1 1 6252F41E
P 15000 6550
F 0 "D2" V 15046 6482 50  0000 R CNN
F 1 "Red_LED" V 14955 6482 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" V 15000 6550 50  0001 C CNN
F 3 "https://www.we-online.de/katalog/datasheet/151031SS06000.pdf" V 15000 6550 50  0001 C CNN
F 4 "$0.17" H 15000 6550 50  0001 C CNN "Item Cost"
F 5 "151031SS06000" H 15000 6550 50  0001 C CNN "MFN"
F 6 "Würth Elektronik" H 15000 6550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 15000 6550 50  0001 C CNN "Supplier"
F 8 "732-5006-ND" H 15000 6550 50  0001 C CNN "Supplier P/N"
F 9 "https://www.digikey.com/en/products/detail/w%C3%BCrth-elektronik/151031SS06000/4489982?s=N4IgTCBcDaIOwGYwFoCsAGdA2ZA5AIiALoC%2BQA" H 15000 6550 50  0001 C CNN "Supplier Link"
	1    15000 6550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15000 6350 15000 6450
$Comp
L Device:R_Small R44
U 1 1 6252F42F
P 15000 6250
F 0 "R44" H 14900 6250 50  0000 C CNN
F 1 "650" V 15000 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 15000 6250 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 15000 6250 50  0001 C CNN
F 4 "$0.13" H 15000 6250 50  0001 C CNN "Item Cost"
F 5 "ERA-6AED6490V" H 15000 6250 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 15000 6250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 15000 6250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERA-6AED6490V/3481342" H 15000 6250 50  0001 C CNN "Supplier Link"
F 9 "P649BNDKR-ND" H 15000 6250 50  0001 C CNN "Supplier P/N"
	1    15000 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	15000 6150 15000 6100
Wire Wire Line
	14650 6100 15000 6100
Connection ~ 14650 6100
Connection ~ 15000 6100
Wire Wire Line
	15000 6100 15150 6100
Wire Wire Line
	15000 6650 15000 6700
$Comp
L power:GND #PWR0124
U 1 1 6252F43F
P 15000 6700
F 0 "#PWR0124" H 15000 6450 50  0001 C CNN
F 1 "GND" H 15100 6700 50  0000 C CNN
F 2 "" H 15000 6700 50  0001 C CNN
F 3 "" H 15000 6700 50  0001 C CNN
	1    15000 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 6100 14400 6100
Wire Wire Line
	14400 6100 14400 6200
$Comp
L Device:C_Small C2
U 1 1 62718D1A
P 14400 6300
F 0 "C2" H 14450 6400 50  0000 L CNN
F 1 "47uF" H 14400 6250 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 14400 6300 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM31CE70G476ME15-01.pdf" H 14400 6300 50  0001 C CNN
F 4 "$0.71" H 14400 6300 50  0001 C CNN "Item Cost"
F 5 "GRM31CE70G476ME15K" H 14400 6300 50  0001 C CNN "MFN"
F 6 "Würth Elektronik" H 14400 6300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 14400 6300 50  0001 C CNN "Supplier"
F 8 "490-GRM31CE70G476ME15KDKR-ND" H 14400 6300 50  0001 C CNN "Supplier P/N"
F 9 "https://www.digikey.com/en/products/detail/murata-electronics/GRM31CE70G476ME15K/13904815" H 14400 6300 50  0001 C CNN "Supplier Link"
	1    14400 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 62718D24
P 14400 6550
F 0 "#PWR0125" H 14400 6300 50  0001 C CNN
F 1 "GND" H 14500 6550 50  0000 C CNN
F 2 "" H 14400 6550 50  0001 C CNN
F 3 "" H 14400 6550 50  0001 C CNN
	1    14400 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	14400 6400 14400 6550
Connection ~ 14400 6100
Wire Wire Line
	14400 6100 14650 6100
$Comp
L power:GND #PWR0126
U 1 1 628324E1
P 14000 6700
F 0 "#PWR0126" H 14000 6450 50  0001 C CNN
F 1 "GND" H 14100 6700 50  0000 C CNN
F 2 "" H 14000 6700 50  0001 C CNN
F 3 "" H 14000 6700 50  0001 C CNN
	1    14000 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 6450 14000 6700
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 6259D206
P 11000 7850
F 0 "H7" H 11100 7899 50  0000 L CNN
F 1 "MountingHole_Pad" H 10250 7800 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 11000 7850 50  0001 C CNN
F 3 "~" H 11000 7850 50  0001 C CNN
	1    11000 7850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 6259DD7D
P 11500 7850
F 0 "H8" H 11600 7899 50  0000 L CNN
F 1 "MountingHole_Pad" H 11600 7808 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 11500 7850 50  0001 C CNN
F 3 "~" H 11500 7850 50  0001 C CNN
	1    11500 7850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 625ECCCE
P 11000 8400
F 0 "H9" H 11100 8449 50  0000 L CNN
F 1 "MountingHole_Pad" H 10250 8350 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 11000 8400 50  0001 C CNN
F 3 "~" H 11000 8400 50  0001 C CNN
	1    11000 8400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 625ECCD8
P 11500 8400
F 0 "H10" H 11600 8449 50  0000 L CNN
F 1 "MountingHole_Pad" H 11600 8358 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 11500 8400 50  0001 C CNN
F 3 "~" H 11500 8400 50  0001 C CNN
	1    11500 8400
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 7950 11250 7950
Wire Wire Line
	11250 7950 11250 8050
Connection ~ 11250 7950
Wire Wire Line
	11250 7950 11500 7950
$Comp
L power:GND #PWR0127
U 1 1 626D66EA
P 11250 8050
F 0 "#PWR0127" H 11250 7800 50  0001 C CNN
F 1 "GND" H 11350 8050 50  0000 C CNN
F 2 "" H 11250 8050 50  0001 C CNN
F 3 "" H 11250 8050 50  0001 C CNN
	1    11250 8050
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 8500 11250 8500
Wire Wire Line
	11250 8500 11250 8600
Connection ~ 11250 8500
Wire Wire Line
	11250 8500 11500 8500
$Comp
L power:GND #PWR0128
U 1 1 627243A3
P 11250 8600
F 0 "#PWR0128" H 11250 8350 50  0001 C CNN
F 1 "GND" H 11350 8600 50  0000 C CNN
F 2 "" H 11250 8600 50  0001 C CNN
F 3 "" H 11250 8600 50  0001 C CNN
	1    11250 8600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J15
U 1 1 624CBF0E
P 10000 5500
F 0 "J15" H 10080 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 10080 5451 50  0000 L CNN
F 2 "Command_Module:7761_ScrewTerminal_RA" H 10000 5500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p69.pdf" H 10000 5500 50  0001 C CNN
F 4 "$0.49" H 10000 5500 50  0001 C CNN "Item Cost"
F 5 "7761" H 10000 5500 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 10000 5500 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/keystone-electronics/7761/2745939?s=N4IgTCBcDaIMwDYC0B2FCCMSByAREAugL5A" H 10000 5500 50  0001 C CNN "Supplier Link"
F 8 "36-7761-ND" H 10000 5500 50  0001 C CNN "Supplier P/N"
F 9 "Keystone Electronics" H 10000 5500 50  0001 C CNN "Manufacturer"
	1    10000 5500
	1    0    0    -1  
$EndComp
NoConn ~ 9800 5500
$Comp
L Connector:Screw_Terminal_01x01 J16
U 1 1 626F0053
P 11500 5500
F 0 "J16" H 11580 5542 50  0000 L CNN
F 1 "Screw_Terminal_01x01" H 11580 5451 50  0000 L CNN
F 2 "Command_Module:7761_ScrewTerminal_RA" H 11500 5500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p69.pdf" H 11500 5500 50  0001 C CNN
F 4 "$0.49" H 11500 5500 50  0001 C CNN "Item Cost"
F 5 "7761" H 11500 5500 50  0001 C CNN "MFN"
F 6 "Digi-Key" H 11500 5500 50  0001 C CNN "Supplier"
F 7 "https://www.digikey.com/en/products/detail/keystone-electronics/7761/2745939?s=N4IgTCBcDaIMwDYC0B2FCCMSByAREAugL5A" H 11500 5500 50  0001 C CNN "Supplier Link"
F 8 "36-7761-ND" H 11500 5500 50  0001 C CNN "Supplier P/N"
F 9 "Keystone Electronics" H 11500 5500 50  0001 C CNN "Manufacturer"
	1    11500 5500
	1    0    0    -1  
$EndComp
NoConn ~ 11300 5500
$EndSCHEMATC
