EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title "Mezzanine Board Schematic"
Date "2022-04-14"
Rev "1.0"
Comp "BU EDF"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Analog_ADC:MCP3208 U2
U 1 1 623B7CA3
P 6950 4550
F 0 "U2" H 6500 5100 50  0000 C CNN
F 1 "MCP3208" H 6600 5000 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 7050 4650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21298c.pdf" H 7050 4650 50  0001 C CNN
F 4 "$6.25" H 6950 4550 50  0001 C CNN "Item Cost"
F 5 "MCP3208-BI/P" H 6950 4550 50  0001 C CNN "MFN"
F 6 "Microchip Technology" H 6950 4550 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 6950 4550 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/microchip-technology/MCP3208-BI-P/319443" H 6950 4550 50  0001 C CNN "Supplier Link"
F 9 "MCP3208-BI/P-ND" H 6950 4550 50  0001 C CNN "Supplier P/N"
	1    6950 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3450 5250 3450
Wire Wire Line
	4750 3550 5350 3550
Wire Wire Line
	4750 3650 5450 3650
Wire Wire Line
	4750 3750 5550 3750
Wire Wire Line
	4750 3850 5650 3850
Wire Wire Line
	4750 3950 5750 3950
Wire Wire Line
	4750 4050 5850 4050
Wire Wire Line
	4750 4150 5950 4150
$Comp
L Connector_Generic:Conn_02x25_Odd_Even J1
U 1 1 623EE2EE
P 4550 4150
F 0 "J1" H 4600 5567 50  0000 C CNN
F 1 "Conn_02x25_Odd_Even" H 4600 5476 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x25_P2.54mm_Vertical" H 4550 4150 50  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE123%20.100%20SFH11%20SERIES%20FEMALE%20HDR%20ST%20RA.pdf" H 4550 4150 50  0001 C CNN
F 4 "$2.42" H 4550 4150 50  0001 C CNN "Item Cost"
F 5 "SFH11-PBPC-D25-ST-BK" H 4550 4150 50  0001 C CNN "MFN"
F 6 "Sullins Connector Solutions" H 4550 4150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 4550 4150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/SFH11-PBPC-D25-ST-BK/1990094" H 4550 4150 50  0001 C CNN "Supplier Link"
F 9 "S9201-ND" H 4550 4150 50  0001 C CNN "Supplier P/N"
	1    4550 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4750 5350 5650 5350
Wire Wire Line
	4750 5250 5750 5250
Wire Wire Line
	4750 5150 5850 5150
Wire Wire Line
	4750 5050 5950 5050
$Comp
L power:GNDA #PWR010
U 1 1 62468554
P 6850 3000
F 0 "#PWR010" H 6850 2750 50  0001 C CNN
F 1 "GNDA" H 6855 2827 50  0000 C CNN
F 2 "" H 6850 3000 50  0001 C CNN
F 3 "" H 6850 3000 50  0001 C CNN
	1    6850 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR011
U 1 1 624691E4
P 6850 5150
F 0 "#PWR011" H 6850 4900 50  0001 C CNN
F 1 "GNDA" H 6950 5150 50  0000 C CNN
F 2 "" H 6850 5150 50  0001 C CNN
F 3 "" H 6850 5150 50  0001 C CNN
	1    6850 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR014
U 1 1 62479065
P 7150 3000
F 0 "#PWR014" H 7150 2750 50  0001 C CNN
F 1 "GNDD" H 7154 2845 50  0000 C CNN
F 2 "" H 7150 3000 50  0001 C CNN
F 3 "" H 7150 3000 50  0001 C CNN
	1    7150 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR016
U 1 1 6247975F
P 7150 5150
F 0 "#PWR016" H 7150 4900 50  0001 C CNN
F 1 "GNDD" H 7250 5150 50  0000 C CNN
F 2 "" H 7150 5150 50  0001 C CNN
F 3 "" H 7150 5150 50  0001 C CNN
	1    7150 5150
	1    0    0    -1  
$EndComp
NoConn ~ 4750 3350
Wire Wire Line
	4750 3250 5100 3250
Wire Wire Line
	5100 3250 5100 3150
Wire Wire Line
	5100 3150 4750 3150
$Comp
L power:+3.3V #PWR07
U 1 1 62482815
P 5100 3150
F 0 "#PWR07" H 5100 3000 50  0001 C CNN
F 1 "+3.3V" H 5115 3323 50  0000 C CNN
F 2 "" H 5100 3150 50  0001 C CNN
F 3 "" H 5100 3150 50  0001 C CNN
	1    5100 3150
	1    0    0    -1  
$EndComp
Connection ~ 5100 3150
Wire Wire Line
	4750 3050 5000 3050
Wire Wire Line
	5000 3050 5000 2950
Wire Wire Line
	5000 2950 4750 2950
$Comp
L power:+12V #PWR06
U 1 1 624855A2
P 5000 2950
F 0 "#PWR06" H 5000 2800 50  0001 C CNN
F 1 "+12V" H 5015 3123 50  0000 C CNN
F 2 "" H 5000 2950 50  0001 C CNN
F 3 "" H 5000 2950 50  0001 C CNN
	1    5000 2950
	1    0    0    -1  
$EndComp
Connection ~ 5000 2950
Wire Wire Line
	4250 2950 4150 2950
Wire Wire Line
	4150 2950 4150 3050
Wire Wire Line
	4150 3050 4250 3050
Wire Wire Line
	4150 3150 4250 3150
Wire Wire Line
	4150 3250 4250 3250
Wire Wire Line
	4150 3350 4250 3350
Wire Wire Line
	4150 3450 4250 3450
Wire Wire Line
	4150 3550 4250 3550
Wire Wire Line
	4150 3650 4250 3650
Wire Wire Line
	4150 3750 4250 3750
Wire Wire Line
	4150 3850 4250 3850
Wire Wire Line
	4150 3950 4250 3950
Wire Wire Line
	4150 4050 4250 4050
Wire Wire Line
	4150 4150 4250 4150
Wire Wire Line
	4150 4250 4250 4250
Wire Wire Line
	4150 4350 4250 4350
Wire Wire Line
	4150 4450 4250 4450
Wire Wire Line
	4150 4550 4250 4550
Wire Wire Line
	4150 4650 4250 4650
Wire Wire Line
	4150 4750 4250 4750
Wire Wire Line
	4150 4850 4250 4850
Wire Wire Line
	4150 4950 4250 4950
Wire Wire Line
	4150 5050 4250 5050
Wire Wire Line
	4150 5150 4250 5150
Wire Wire Line
	4150 5250 4250 5250
Wire Wire Line
	4150 5350 4250 5350
Connection ~ 4150 3050
Wire Wire Line
	4150 3050 4150 3150
Connection ~ 4150 3150
Wire Wire Line
	4150 3150 4150 3250
Connection ~ 4150 3250
Wire Wire Line
	4150 3250 4150 3350
Connection ~ 4150 3350
Wire Wire Line
	4150 3350 4150 3450
Connection ~ 4150 3450
Wire Wire Line
	4150 3450 4150 3550
Connection ~ 4150 3550
Wire Wire Line
	4150 3550 4150 3650
Connection ~ 4150 3650
Wire Wire Line
	4150 3650 4150 3750
Connection ~ 4150 3750
Wire Wire Line
	4150 3750 4150 3850
Connection ~ 4150 3850
Wire Wire Line
	4150 3850 4150 3950
Connection ~ 4150 3950
Wire Wire Line
	4150 3950 4150 4050
Connection ~ 4150 4050
Wire Wire Line
	4150 4050 4150 4150
Connection ~ 4150 4150
Wire Wire Line
	4150 4150 4150 4250
Connection ~ 4150 4250
Wire Wire Line
	4150 4250 4150 4350
Connection ~ 4150 4350
Wire Wire Line
	4150 4350 4150 4450
Connection ~ 4150 4450
Wire Wire Line
	4150 4450 4150 4550
Connection ~ 4150 4550
Wire Wire Line
	4150 4550 4150 4650
Connection ~ 4150 4650
Wire Wire Line
	4150 4650 4150 4750
Connection ~ 4150 4750
Wire Wire Line
	4150 4750 4150 4850
Connection ~ 4150 4850
Wire Wire Line
	4150 4850 4150 4950
Connection ~ 4150 4950
Wire Wire Line
	4150 4950 4150 5050
Connection ~ 4150 5050
Wire Wire Line
	4150 5050 4150 5150
Connection ~ 4150 5150
Wire Wire Line
	4150 5150 4150 5250
Connection ~ 4150 5250
Wire Wire Line
	4150 5250 4150 5350
Connection ~ 4150 5350
Wire Wire Line
	4150 5350 4150 5400
$Comp
L power:GND #PWR01
U 1 1 624CA9E9
P 4150 5400
F 0 "#PWR01" H 4150 5150 50  0001 C CNN
F 1 "GND" H 4155 5227 50  0000 C CNN
F 2 "" H 4150 5400 50  0001 C CNN
F 3 "" H 4150 5400 50  0001 C CNN
	1    4150 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 7150 5850 7150
Wire Wire Line
	5850 7150 5850 6800
Wire Wire Line
	4650 7250 5950 7250
Wire Wire Line
	5950 7250 5950 6900
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 624DBE7D
P 4450 7150
F 0 "J3" H 4550 7450 50  0000 C CNN
F 1 "Conn_01x03" H 4550 7350 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 4450 7150 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Sullins%20PDFs/Female_Headers.100_DS.pdf" H 4450 7150 50  0001 C CNN
F 4 "$0.37" H 4450 7150 50  0001 C CNN "Item Cost"
F 5 "PPPC031LFBN-RC" H 4450 7150 50  0001 C CNN "MFN"
F 6 "Sullins Connector Solutions" H 4450 7150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 4450 7150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/PPPC031LFBN-RC/810175" H 4450 7150 50  0001 C CNN "Supplier Link"
F 9 "S7036-ND" H 4450 7150 50  0001 C CNN "Supplier P/N"
	1    4450 7150
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 624D752B
P 4450 6600
F 0 "J2" H 4500 6900 50  0000 C CNN
F 1 "Conn_01x03" H 4500 6800 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 4450 6600 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Sullins%20PDFs/Female_Headers.100_DS.pdf" H 4450 6600 50  0001 C CNN
F 4 "$0.37" H 4450 6600 50  0001 C CNN "Item Cost"
F 5 "PPPC031LFBN-RC" H 4450 6600 50  0001 C CNN "MFN"
F 6 "Sullins Connector Solutions" H 4450 6600 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 4450 6600 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/PPPC031LFBN-RC/810175" H 4450 6600 50  0001 C CNN "Supplier Link"
F 9 "S7036-ND" H 4450 6600 50  0001 C CNN "Supplier P/N"
	1    4450 6600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 6500 4700 6500
$Comp
L power:GND #PWR02
U 1 1 62526716
P 4700 6500
F 0 "#PWR02" H 4700 6250 50  0001 C CNN
F 1 "GND" H 4705 6327 50  0000 C CNN
F 2 "" H 4700 6500 50  0001 C CNN
F 3 "" H 4700 6500 50  0001 C CNN
	1    4700 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4650 7050 4700 7050
$Comp
L power:GND #PWR03
U 1 1 6252E9A2
P 4700 7050
F 0 "#PWR03" H 4700 6800 50  0001 C CNN
F 1 "GND" H 4705 6877 50  0000 C CNN
F 2 "" H 4700 7050 50  0001 C CNN
F 3 "" H 4700 7050 50  0001 C CNN
	1    4700 7050
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 6257FBE6
P 14000 8350
F 0 "H1" H 14100 8399 50  0000 L CNN
F 1 "MountingHole_Pad" H 13250 8300 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 14000 8350 50  0001 C CNN
F 3 "~" H 14000 8350 50  0001 C CNN
	1    14000 8350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 6258241B
P 14500 8350
F 0 "H2" H 14600 8399 50  0000 L CNN
F 1 "MountingHole_Pad" H 14600 8308 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 14500 8350 50  0001 C CNN
F 3 "~" H 14500 8350 50  0001 C CNN
	1    14500 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	14250 8550 14250 8450
Connection ~ 14250 8450
$Comp
L power:GND #PWR028
U 1 1 625A2AAA
P 14250 8550
F 0 "#PWR028" H 14250 8300 50  0001 C CNN
F 1 "GND" H 14255 8377 50  0000 C CNN
F 2 "" H 14250 8550 50  0001 C CNN
F 3 "" H 14250 8550 50  0001 C CNN
	1    14250 8550
	1    0    0    -1  
$EndComp
Text Label 4750 3450 0    50   ~ 0
PWR_GOOD
Text Label 4750 3550 0    50   ~ 0
TMS
Text Label 4750 3650 0    50   ~ 0
TDO
Text Label 4750 3750 0    50   ~ 0
TX_IPMC
Text Label 4750 3850 0    50   ~ 0
TX_ZYNQ
Text Label 4750 3950 0    50   ~ 0
SENSE_SDA
Text Label 4750 4050 0    50   ~ 0
PWR_EN
Text Label 4750 4150 0    50   ~ 0
FPGA_GPIO0
Text Label 4750 4250 0    50   ~ 0
FPGA_GPIO1
Text Label 4750 4350 0    50   ~ 0
FPGA_GPIO2
Text Label 4750 4450 0    50   ~ 0
CPLD_GPIO0
Text Label 4750 4550 0    50   ~ 0
CPLD_GPIO1
Text Label 4750 4650 0    50   ~ 0
EN
Text Label 4750 4750 0    50   ~ 0
TCK
Text Label 4750 4850 0    50   ~ 0
TDI
Text Label 4750 4950 0    50   ~ 0
RX_IPMC
Text Label 4750 5050 0    50   ~ 0
RX_ZYNQ
Text Label 4750 5150 0    50   ~ 0
SENSE_SCL
Text Label 4750 5250 0    50   ~ 0
MON_RX
Text Label 4750 5350 0    50   ~ 0
PS_RST
Text Label 4650 7150 0    50   ~ 0
LHC_CLK_P
Text Label 4650 7250 0    50   ~ 0
LHC_CLK_N
Text Label 4650 6600 0    50   ~ 0
HQ_CLK_P
Text Label 4650 6700 0    50   ~ 0
HQ_CLK_N
Wire Wire Line
	14000 8450 14250 8450
Wire Wire Line
	14250 8450 14500 8450
$Comp
L Device:LED_Small D1
U 1 1 625DE928
P 5550 8650
F 0 "D1" H 5550 8885 50  0000 C CNN
F 1 "Red" H 5550 8794 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" V 5550 8650 50  0001 C CNN
F 3 "https://www.lumex.com/spec/SSF-LXH103ID.pdf" V 5550 8650 50  0001 C CNN
F 4 "$0.73" H 5550 8650 50  0001 C CNN "Item Cost"
F 5 "SSF-LXH103ID" H 5550 8650 50  0001 C CNN "MFN"
F 6 "Lumex Opto/Components Inc." H 5550 8650 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 5550 8650 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SSF-LXH103ID/145298" H 5550 8650 50  0001 C CNN "Supplier Link"
F 9 "67-1235-ND" H 5550 8650 50  0001 C CNN "Supplier P/N"
	1    5550 8650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5650 8650 5800 8650
Wire Wire Line
	6000 8650 6300 8650
$Comp
L power:+12V #PWR04
U 1 1 6262CB26
P 5050 8600
F 0 "#PWR04" H 5050 8450 50  0001 C CNN
F 1 "+12V" H 5065 8773 50  0000 C CNN
F 2 "" H 5050 8600 50  0001 C CNN
F 3 "" H 5050 8600 50  0001 C CNN
	1    5050 8600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 8650 5450 8650
$Comp
L Device:LED_Small D2
U 1 1 6264F0F7
P 5550 9150
F 0 "D2" H 5550 9385 50  0000 C CNN
F 1 "Red" H 5550 9294 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" V 5550 9150 50  0001 C CNN
F 3 "https://www.lumex.com/spec/SSF-LXH103ID.pdf" V 5550 9150 50  0001 C CNN
F 4 "$0.73" H 5550 9150 50  0001 C CNN "Item Cost"
F 5 "SSF-LXH103ID" H 5550 9150 50  0001 C CNN "MFN"
F 6 "Lumex Opto/Components Inc." H 5550 9150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 5550 9150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SSF-LXH103ID/145298" H 5550 9150 50  0001 C CNN "Supplier Link"
F 9 "67-1235-ND" H 5550 9150 50  0001 C CNN "Supplier P/N"
	1    5550 9150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5650 9150 5800 9150
Wire Wire Line
	6000 9150 6300 9150
Wire Wire Line
	5050 9150 5450 9150
$Comp
L power:+3.3V #PWR05
U 1 1 6267EA24
P 5050 9100
F 0 "#PWR05" H 5050 8950 50  0001 C CNN
F 1 "+3.3V" H 5065 9273 50  0000 C CNN
F 2 "" H 5050 9100 50  0001 C CNN
F 3 "" H 5050 9100 50  0001 C CNN
	1    5050 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 9250 6300 9150
$Comp
L power:GND #PWR09
U 1 1 626A1525
P 6300 9250
F 0 "#PWR09" H 6300 9000 50  0001 C CNN
F 1 "GND" H 6305 9077 50  0000 C CNN
F 2 "" H 6300 9250 50  0001 C CNN
F 3 "" H 6300 9250 50  0001 C CNN
	1    6300 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 8750 6300 8650
$Comp
L power:GND #PWR08
U 1 1 626B5F75
P 6300 8750
F 0 "#PWR08" H 6300 8500 50  0001 C CNN
F 1 "GND" H 6305 8577 50  0000 C CNN
F 2 "" H 6300 8750 50  0001 C CNN
F 3 "" H 6300 8750 50  0001 C CNN
	1    6300 8750
	1    0    0    -1  
$EndComp
$Comp
L Mezzanine_Board:FT232RL J4
U 1 1 626BC8CD
P 14350 5400
F 0 "J4" H 14350 5750 50  0000 C CNN
F 1 "FT232RL" H 14350 5650 50  0000 C CNN
F 2 "Mezzanine_Board:FT232RL" H 14450 5400 50  0001 C CNN
F 3 "https://cdn.sparkfun.com/datasheets/BreakoutBoards/DS_FT232R.pdf" H 14450 5400 50  0001 C CNN
F 4 "$15.95" H 14350 5400 50  0001 C CNN "Item Cost"
F 5 "BOB-12731" H 14350 5400 50  0001 C CNN "MFN"
F 6 "SparkFun Electronics" H 14350 5400 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 14350 5400 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sparkfun-electronics/BOB-12731/5673781?utm_adgroup=Integrated%20Circuits&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_RLSA&utm_term=&utm_content=Integrated%20Circuits&gclid=EAIaIQobChMI9Kvq7Oyx9wIVGJfICh3T2g3REAAYASAAEgI4mPD_BwE" H 14350 5400 50  0001 C CNN "Supplier Link"
F 9 "1568-1195-ND" H 14350 5400 50  0001 C CNN "Supplier P/N"
	1    14350 5400
	1    0    0    -1  
$EndComp
$Comp
L Mezzanine_Board:FT232RL J4
U 2 1 626BD222
P 8350 9100
F 0 "J4" H 8350 9750 50  0000 C CNN
F 1 "FT232RL" H 8350 9650 50  0000 C CNN
F 2 "Mezzanine_Board:FT232RL" H 8450 9100 50  0001 C CNN
F 3 "https://cdn.sparkfun.com/datasheets/BreakoutBoards/DS_FT232R.pdf" H 8450 9100 50  0001 C CNN
F 4 "$15.95" H 8350 9100 50  0001 C CNN "Item Cost"
F 5 "BOB-12731" H 8350 9100 50  0001 C CNN "MFN"
F 6 "SparkFun Electronics" H 8350 9100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 8350 9100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sparkfun-electronics/BOB-12731/5673781?utm_adgroup=Integrated%20Circuits&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_RLSA&utm_term=&utm_content=Integrated%20Circuits&gclid=EAIaIQobChMI9Kvq7Oyx9wIVGJfICh3T2g3REAAYASAAEgI4mPD_BwE" H 8350 9100 50  0001 C CNN "Supplier Link"
F 9 "1568-1195-ND" H 8350 9100 50  0001 C CNN "Supplier P/N"
	2    8350 9100
	1    0    0    -1  
$EndComp
NoConn ~ 9000 8700
NoConn ~ 9000 8800
NoConn ~ 9000 8900
NoConn ~ 9000 9000
NoConn ~ 9000 9100
NoConn ~ 9000 9200
NoConn ~ 9000 9300
NoConn ~ 9000 9400
NoConn ~ 9000 9500
NoConn ~ 8550 8700
NoConn ~ 8550 8800
NoConn ~ 8550 8900
NoConn ~ 8550 9000
NoConn ~ 8550 9100
NoConn ~ 8550 9200
NoConn ~ 8550 9300
NoConn ~ 8550 9400
NoConn ~ 8550 9500
Wire Wire Line
	4750 4250 6350 4250
Wire Wire Line
	4750 4350 6350 4350
Wire Wire Line
	4750 4450 6350 4450
Wire Wire Line
	4750 4550 6350 4550
Wire Wire Line
	4750 4650 6350 4650
Wire Wire Line
	4750 4750 6350 4750
Wire Wire Line
	4750 4850 6350 4850
Wire Wire Line
	4750 4950 6350 4950
Wire Wire Line
	5250 2100 6350 2100
Wire Wire Line
	5350 2200 6350 2200
Wire Wire Line
	5450 2300 6350 2300
Wire Wire Line
	5550 2400 6350 2400
Wire Wire Line
	5650 2500 6350 2500
Wire Wire Line
	5750 2600 6350 2600
Wire Wire Line
	5850 2700 6350 2700
Wire Wire Line
	5950 2800 6350 2800
Connection ~ 7950 4450
Connection ~ 7950 3800
Text Label 15100 5450 2    50   ~ 0
USB_IN
Text Label 15100 5350 2    50   ~ 0
USB_OUT
Wire Wire Line
	14650 5450 15100 5450
Wire Wire Line
	14650 5350 15100 5350
$Comp
L power:GND #PWR032
U 1 1 628A451C
P 14650 5650
F 0 "#PWR032" H 14650 5400 50  0001 C CNN
F 1 "GND" H 14655 5477 50  0000 C CNN
F 2 "" H 14650 5650 50  0001 C CNN
F 3 "" H 14650 5650 50  0001 C CNN
	1    14650 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	14650 5650 14650 5550
$Comp
L Mezzanine_Board:FT232RL J4
U 3 1 626A840A
P 8800 9100
F 0 "J4" H 8800 9750 50  0000 C CNN
F 1 "FT232RL" H 8800 9650 50  0000 C CNN
F 2 "Mezzanine_Board:FT232RL" H 8900 9100 50  0001 C CNN
F 3 "https://cdn.sparkfun.com/datasheets/BreakoutBoards/DS_FT232R.pdf" H 8900 9100 50  0001 C CNN
F 4 "$15.95" H 8800 9100 50  0001 C CNN "Item Cost"
F 5 "BOB-12731" H 8800 9100 50  0001 C CNN "MFN"
F 6 "SparkFun Electronics" H 8800 9100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 8800 9100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sparkfun-electronics/BOB-12731/5673781?utm_adgroup=Integrated%20Circuits&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_RLSA&utm_term=&utm_content=Integrated%20Circuits&gclid=EAIaIQobChMI9Kvq7Oyx9wIVGJfICh3T2g3REAAYASAAEgI4mPD_BwE" H 8800 9100 50  0001 C CNN "Supplier Link"
F 9 "1568-1195-ND" H 8800 9100 50  0001 C CNN "Supplier P/N"
	3    8800 9100
	1    0    0    -1  
$EndComp
Connection ~ 12300 6200
$Comp
L Device:C_Small C4
U 1 1 629BB2CD
P 10150 2400
F 0 "C4" V 10000 2400 50  0000 C CNN
F 1 "25" V 10050 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10150 2400 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21B104KACNNNC_Spec.pdf" H 10150 2400 50  0001 C CNN
F 4 "$0.1" H 10150 2400 50  0001 C CNN "Item Cost"
F 5 "CL21B104KACNNNC" H 10150 2400 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 10150 2400 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 10150 2400 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B104KACNNNC/3886757" H 10150 2400 50  0001 C CNN "Supplier Link"
F 9 "1276-1099-6-ND" H 10150 2400 50  0001 C CNN "Supplier P/N"
	1    10150 2400
	0    -1   1    0   
$EndComp
Wire Wire Line
	9950 2400 10050 2400
$Comp
L power:GND #PWR022
U 1 1 629BB2C2
P 9950 2400
F 0 "#PWR022" H 9950 2150 50  0001 C CNN
F 1 "GND" H 9955 2227 50  0000 C CNN
F 2 "" H 9950 2400 50  0001 C CNN
F 3 "" H 9950 2400 50  0001 C CNN
	1    9950 2400
	0    1    -1   0   
$EndComp
Wire Wire Line
	10350 2400 10250 2400
$Comp
L Device:C_Small C5
U 1 1 62792F8F
P 10650 2400
F 0 "C5" V 10421 2400 50  0000 C CNN
F 1 "25" V 10512 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10650 2400 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21B104KACNNNC_Spec.pdf" H 10650 2400 50  0001 C CNN
F 4 "$0.1" H 10650 2400 50  0001 C CNN "Item Cost"
F 5 "CL21B104KACNNNC" H 10650 2400 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 10650 2400 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 10650 2400 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B104KACNNNC/3886757" H 10650 2400 50  0001 C CNN "Supplier Link"
F 9 "1276-1099-6-ND" H 10650 2400 50  0001 C CNN "Supplier P/N"
	1    10650 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	10850 2400 10750 2400
$Comp
L power:GND #PWR025
U 1 1 62792F9A
P 10850 2400
F 0 "#PWR025" H 10850 2150 50  0001 C CNN
F 1 "GND" H 10855 2227 50  0000 C CNN
F 2 "" H 10850 2400 50  0001 C CNN
F 3 "" H 10850 2400 50  0001 C CNN
	1    10850 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10450 2400 10550 2400
Wire Wire Line
	10450 2400 10450 2500
Wire Wire Line
	10450 2100 10450 2400
Connection ~ 10450 2400
Connection ~ 10450 2100
Wire Wire Line
	10350 2100 10450 2100
Wire Wire Line
	10450 2000 10450 2100
$Comp
L Device:R_Small R19
U 1 1 6291BDD5
P 10350 2250
F 0 "R19" H 10450 2200 20  0000 C CNN
F 1 "100" V 10350 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10350 2250 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1773204-3&DocType=DS&DocLang=English" H 10350 2250 50  0001 C CNN
F 4 "$0.1" H 10350 2250 50  0001 C CNN "Item Cost"
F 5 "CRGCQ0805J100R" H 10350 2250 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 10350 2250 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 10350 2250 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRGCQ0805J100R/8576730" H 10350 2250 50  0001 C CNN "Supplier Link"
F 9 "A130128DKR-ND" H 10350 2250 50  0001 C CNN "Supplier P/N"
	1    10350 2250
	-1   0    0    1   
$EndComp
Connection ~ 14400 3300
Wire Wire Line
	14400 3300 14400 3400
Wire Wire Line
	14400 3300 14500 3300
$Comp
L power:GND #PWR033
U 1 1 627E388F
P 14800 3300
F 0 "#PWR033" H 14800 3050 50  0001 C CNN
F 1 "GND" H 14805 3127 50  0000 C CNN
F 2 "" H 14800 3300 50  0001 C CNN
F 3 "" H 14800 3300 50  0001 C CNN
	1    14800 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14800 3300 14700 3300
$Comp
L Device:C_Small C6
U 1 1 627E3884
P 14600 3300
F 0 "C6" V 14371 3300 50  0000 C CNN
F 1 "25" V 14462 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 14600 3300 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21B104KACNNNC_Spec.pdf" H 14600 3300 50  0001 C CNN
F 4 "$0.1" H 14600 3300 50  0001 C CNN "Item Cost"
F 5 "CL21B104KACNNNC" H 14600 3300 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 14600 3300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 14600 3300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B104KACNNNC/3886757" H 14600 3300 50  0001 C CNN "Supplier Link"
F 9 "1276-1099-6-ND" H 14600 3300 50  0001 C CNN "Supplier P/N"
	1    14600 3300
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR029
U 1 1 627E387A
P 14400 3200
F 0 "#PWR029" H 14400 3050 50  0001 C CNN
F 1 "+3.3V" H 14415 3373 50  0000 C CNN
F 2 "" H 14400 3200 50  0001 C CNN
F 3 "" H 14400 3200 50  0001 C CNN
	1    14400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	14400 3300 14400 3200
$Comp
L power:+3.3V #PWR023
U 1 1 62792F85
P 10450 2000
F 0 "#PWR023" H 10450 1850 50  0001 C CNN
F 1 "+3.3V" H 10450 2150 50  0000 C CNN
F 2 "" H 10450 2000 50  0001 C CNN
F 3 "" H 10450 2000 50  0001 C CNN
	1    10450 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D3
U 1 1 62569FF4
P 12100 5900
F 0 "D3" H 11950 5950 50  0000 C CNN
F 1 "Blue" H 12350 5950 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" V 12100 5900 50  0001 C CNN
F 3 "https://www.lumex.com/spec/SSF-LXH103USBD.pdf" V 12100 5900 50  0001 C CNN
F 4 "$2.49" H 12100 5900 50  0001 C CNN "Item Cost"
F 5 "SSF-LXH103USBD" H 12100 5900 50  0001 C CNN "MFN"
F 6 "Lumex Opto/Components Inc." H 12100 5900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12100 5900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SSF-LXH103USBD/516767" H 12100 5900 50  0001 C CNN "Supplier Link"
F 9 "67-2028-ND" H 12100 5900 50  0001 C CNN "Supplier P/N"
	1    12100 5900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R29
U 1 1 62569FFE
P 11500 5900
F 0 "R29" V 11450 5700 50  0000 C CNN
F 1 "56" V 11500 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11500 5900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1773204-3&DocType=DS&DocLang=English" H 11500 5900 50  0001 C CNN
F 4 "$0.1" H 11500 5900 50  0001 C CNN "Item Cost"
F 5 "CRGCQ0805F56R" H 11500 5900 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11500 5900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11500 5900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRGCQ0805F56R/8576336" H 11500 5900 50  0001 C CNN "Supplier Link"
F 9 "A129734DKR-ND" H 11500 5900 50  0001 C CNN "Supplier P/N"
	1    11500 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	12200 5900 12300 5900
$Comp
L Device:LED_Small D4
U 1 1 625B1299
P 12100 6000
F 0 "D4" H 11950 6050 50  0000 C CNN
F 1 "Green" H 12350 6050 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" V 12100 6000 50  0001 C CNN
F 3 "https://www.lumex.com/spec/SSF-LXH103GD.pdf" V 12100 6000 50  0001 C CNN
F 4 "$0.75" H 12100 6000 50  0001 C CNN "Item Cost"
F 5 "SSF-LXH103GD" H 12100 6000 50  0001 C CNN "MFN"
F 6 "Lumex Opto/Components Inc." H 12100 6000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12100 6000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SSF-LXH103GD/144870" H 12100 6000 50  0001 C CNN "Supplier Link"
F 9 "67-1236-ND" H 12100 6000 50  0001 C CNN "Supplier P/N"
	1    12100 6000
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R30
U 1 1 625B12A3
P 11500 6000
F 0 "R30" V 11450 5800 50  0000 C CNN
F 1 "240" V 11500 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11500 6000 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ241V" H 11500 6000 50  0001 C CNN
F 4 "$0.1" H 11500 6000 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ241V" H 11500 6000 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 11500 6000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11500 6000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ241V/89709" H 11500 6000 50  0001 C CNN "Supplier Link"
F 9 "P240ADKR-ND" H 11500 6000 50  0001 C CNN "Supplier P/N"
	1    11500 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	12200 6000 12300 6000
$Comp
L Device:LED_Small D5
U 1 1 625B12B0
P 12100 6100
F 0 "D5" H 11950 6150 50  0000 C CNN
F 1 "White" H 12350 6150 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" V 12100 6100 50  0001 C CNN
F 3 "https://www.lumex.com/spec/SSF-LXH103UWC.pdf" V 12100 6100 50  0001 C CNN
F 4 "$2.03" H 12100 6100 50  0001 C CNN "Item Cost"
F 5 "SSF-LXH103UWC" H 12100 6100 50  0001 C CNN "MFN"
F 6 "Lumex Opto/Components Inc." H 12100 6100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12100 6100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SSF-LXH103UWC/1109132" H 12100 6100 50  0001 C CNN "Supplier Link"
F 9 "67-2026-ND" H 12100 6100 50  0001 C CNN "Supplier P/N"
	1    12100 6100
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R31
U 1 1 625B12BA
P 11500 6100
F 0 "R31" V 11450 5900 50  0000 C CNN
F 1 "56" V 11500 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11500 6100 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1773204-3&DocType=DS&DocLang=English" H 11500 6100 50  0001 C CNN
F 4 "$0.1" H 11500 6100 50  0001 C CNN "Item Cost"
F 5 "CRGCQ0805F56R" H 11500 6100 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11500 6100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11500 6100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRGCQ0805F56R/8576336" H 11500 6100 50  0001 C CNN "Supplier Link"
F 9 "A129734DKR-ND" H 11500 6100 50  0001 C CNN "Supplier P/N"
	1    11500 6100
	0    1    1    0   
$EndComp
Wire Wire Line
	12200 6100 12300 6100
$Comp
L Device:LED_Small D6
U 1 1 625C494E
P 12100 6200
F 0 "D6" H 11950 6250 50  0000 C CNN
F 1 "Yellow" H 12350 6250 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" V 12100 6200 50  0001 C CNN
F 3 "https://www.lumex.com/spec/SSF-LXH103YD.pdf" V 12100 6200 50  0001 C CNN
F 4 "$0.72" H 12100 6200 50  0001 C CNN "Item Cost"
F 5 "SSF-LXH103YD" H 12100 6200 50  0001 C CNN "MFN"
F 6 "Lumex Opto/Components Inc." H 12100 6200 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 12100 6200 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/lumex-opto-components-inc/SSF-LXH103YD/144873" H 12100 6200 50  0001 C CNN "Supplier Link"
F 9 "67-1237-ND" H 12100 6200 50  0001 C CNN "Supplier P/N"
	1    12100 6200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R32
U 1 1 625C4958
P 11500 6200
F 0 "R32" V 11450 6000 50  0000 C CNN
F 1 "240" V 11500 6200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11500 6200 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ241V" H 11500 6200 50  0001 C CNN
F 4 "$0.1" H 11500 6200 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ241V" H 11500 6200 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 11500 6200 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11500 6200 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ241V/89709" H 11500 6200 50  0001 C CNN "Supplier Link"
F 9 "P240ADKR-ND" H 11500 6200 50  0001 C CNN "Supplier P/N"
	1    11500 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	12200 6200 12300 6200
$Comp
L power:GND #PWR027
U 1 1 62652616
P 12300 6200
F 0 "#PWR027" H 12300 5950 50  0001 C CNN
F 1 "GND" H 12300 6050 50  0000 C CNN
F 2 "" H 12300 6200 50  0001 C CNN
F 3 "" H 12300 6200 50  0001 C CNN
	1    12300 6200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12300 5900 12300 6000
Wire Wire Line
	12300 6000 12300 6100
Connection ~ 12300 6000
Connection ~ 12300 6100
Wire Wire Line
	12300 6100 12300 6200
$Comp
L Device:R_Small R20
U 1 1 62770825
P 11300 5300
F 0 "R20" V 11250 5400 20  0000 C CNN
F 1 "1k" V 11300 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 5300 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 5300 50  0001 C CNN
F 4 "$0.1" H 11300 5300 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 5300 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 5300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 5300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 5300 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 5300 50  0001 C CNN "Supplier P/N"
	1    11300 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12050 5300 11400 5300
$Comp
L power:GND #PWR030
U 1 1 625A91CB
P 14400 4400
F 0 "#PWR030" H 14400 4150 50  0001 C CNN
F 1 "GND" H 14405 4227 50  0000 C CNN
F 2 "" H 14400 4400 50  0001 C CNN
F 3 "" H 14400 4400 50  0001 C CNN
	1    14400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	14400 4400 14400 4300
$Comp
L power:+3.3V #PWR026
U 1 1 6258F030
P 11650 4200
F 0 "#PWR026" H 11650 4050 50  0001 C CNN
F 1 "+3.3V" H 11665 4373 50  0000 C CNN
F 2 "" H 11650 4200 50  0001 C CNN
F 3 "" H 11650 4200 50  0001 C CNN
	1    11650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	11550 4200 11650 4200
Text Label 11300 4200 2    50   ~ 0
RESET
$Comp
L Device:R_Small R28
U 1 1 6255621E
P 11450 4200
F 0 "R28" V 11400 4300 20  0000 C CNN
F 1 "1k" V 11450 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11450 4200 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11450 4200 50  0001 C CNN
F 4 "$0.1" H 11450 4200 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11450 4200 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11450 4200 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11450 4200 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11450 4200 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11450 4200 50  0001 C CNN "Supplier P/N"
	1    11450 4200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:AVR-ISP-6 J6
U 1 1 623E4DFD
P 14500 3900
F 0 "J6" H 14171 3996 50  0000 R CNN
F 1 "AVR-ISP-6" H 14171 3905 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 14250 3950 50  0001 C CNN
F 3 " https://s3.amazonaws.com/catalogspreads-pdf/PAGE122%20.100%20SBH11%20SERIES%20MALE%20BOX%20HDR%20ST%20RA%20SMT.pdf" H 13225 3350 50  0001 C CNN
F 4 "$0.75" H 14500 3900 50  0001 C CNN "Item Cost"
F 5 "SBH11-NBPC-D03-ST-BK" H 14500 3900 50  0001 C CNN "MFN"
F 6 "Sullins Connector Solutions" H 14500 3900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 14500 3900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/SBH11-NBPC-D03-ST-BK/4558573" H 14500 3900 50  0001 C CNN "Supplier Link"
F 9 "S9716-ND" H 14500 3900 50  0001 C CNN "Supplier P/N"
	1    14500 3900
	1    0    0    -1  
$EndComp
Text Label 15150 4000 2    50   ~ 0
RESET
Wire Wire Line
	12050 4700 11400 4700
Wire Wire Line
	12050 4600 11400 4600
Wire Wire Line
	11200 4600 11050 4600
$Comp
L Device:R_Small R27
U 1 1 62836D13
P 11300 4600
F 0 "R27" V 11250 4700 20  0000 C CNN
F 1 "1k" V 11300 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 4600 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 4600 50  0001 C CNN
F 4 "$0.1" H 11300 4600 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 4600 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 4600 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 4600 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 4600 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 4600 50  0001 C CNN "Supplier P/N"
	1    11300 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11200 4700 11050 4700
$Comp
L Device:R_Small R26
U 1 1 62836D08
P 11300 4700
F 0 "R26" V 11250 4800 20  0000 C CNN
F 1 "1k" V 11300 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 4700 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 4700 50  0001 C CNN
F 4 "$0.1" H 11300 4700 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 4700 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 4700 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 4700 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 4700 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 4700 50  0001 C CNN "Supplier P/N"
	1    11300 4700
	0    -1   -1   0   
$EndComp
Text Label 12050 4600 2    50   ~ 0
LHC_CLK_N
Text Label 12050 4700 2    50   ~ 0
LHC_CLK_P
Wire Wire Line
	12050 4900 11400 4900
Wire Wire Line
	12050 4800 11400 4800
Wire Wire Line
	11200 4800 11050 4800
$Comp
L Device:R_Small R25
U 1 1 627DE6A2
P 11300 4800
F 0 "R25" V 11250 4900 20  0000 C CNN
F 1 "1k" V 11300 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 4800 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 4800 50  0001 C CNN
F 4 "$0.1" H 11300 4800 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 4800 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 4800 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 4800 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 4800 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 4800 50  0001 C CNN "Supplier P/N"
	1    11300 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11200 4900 11050 4900
$Comp
L Device:R_Small R24
U 1 1 627DE697
P 11300 4900
F 0 "R24" V 11250 5000 20  0000 C CNN
F 1 "1k" V 11300 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 4900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 4900 50  0001 C CNN
F 4 "$0.1" H 11300 4900 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 4900 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 4900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 4900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 4900 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 4900 50  0001 C CNN "Supplier P/N"
	1    11300 4900
	0    -1   -1   0   
$EndComp
Text Label 12050 4800 2    50   ~ 0
HQ_CLK_N
Text Label 12050 4900 2    50   ~ 0
HQ_CLK_P
Wire Wire Line
	12050 5200 11400 5200
Wire Wire Line
	12050 5100 11400 5100
Wire Wire Line
	12050 5000 11400 5000
Wire Wire Line
	8850 5300 9500 5300
Wire Wire Line
	8850 5200 9500 5200
Wire Wire Line
	8850 5100 9500 5100
Wire Wire Line
	8850 5000 9500 5000
Wire Wire Line
	8850 4900 9500 4900
Wire Wire Line
	8850 4800 9500 4800
Wire Wire Line
	8850 4700 9500 4700
Wire Wire Line
	8850 4600 9500 4600
Wire Wire Line
	8850 6200 9500 6200
Wire Wire Line
	8850 6100 9500 6100
Wire Wire Line
	8850 6000 9500 6000
Wire Wire Line
	8850 5900 9500 5900
Wire Wire Line
	8850 5800 9500 5800
Wire Wire Line
	8850 5700 9500 5700
Wire Wire Line
	8850 5600 9500 5600
Wire Wire Line
	8850 5500 9500 5500
Wire Wire Line
	11200 5000 11050 5000
$Comp
L Device:R_Small R23
U 1 1 62770849
P 11300 5000
F 0 "R23" V 11250 5100 20  0000 C CNN
F 1 "1k" V 11300 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 5000 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 5000 50  0001 C CNN
F 4 "$0.1" H 11300 5000 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 5000 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 5000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 5000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 5000 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 5000 50  0001 C CNN "Supplier P/N"
	1    11300 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11200 5100 11050 5100
$Comp
L Device:R_Small R22
U 1 1 6277083D
P 11300 5100
F 0 "R22" V 11250 5200 20  0000 C CNN
F 1 "1k" V 11300 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 5100 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 5100 50  0001 C CNN
F 4 "$0.1" H 11300 5100 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 5100 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 5100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 5100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 5100 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 5100 50  0001 C CNN "Supplier P/N"
	1    11300 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11200 5200 11050 5200
$Comp
L Device:R_Small R21
U 1 1 62770831
P 11300 5200
F 0 "R21" V 11250 5300 20  0000 C CNN
F 1 "1k" V 11300 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11300 5200 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 11300 5200 50  0001 C CNN
F 4 "$0.1" H 11300 5200 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 11300 5200 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 11300 5200 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 11300 5200 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 11300 5200 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 11300 5200 50  0001 C CNN "Supplier P/N"
	1    11300 5200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11200 5300 11050 5300
Wire Wire Line
	9700 5300 9850 5300
$Comp
L Device:R_Small R10
U 1 1 62761A52
P 9600 5300
F 0 "R10" V 9550 5400 20  0000 C CNN
F 1 "1k" V 9600 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5300 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5300 50  0001 C CNN
F 4 "$0.1" H 9600 5300 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5300 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5300 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5300 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5300 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5300 50  0001 C CNN "Supplier P/N"
	1    9600 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5200 9850 5200
$Comp
L Device:R_Small R9
U 1 1 62761A46
P 9600 5200
F 0 "R9" V 9550 5300 20  0000 C CNN
F 1 "1k" V 9600 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5200 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5200 50  0001 C CNN
F 4 "$0.1" H 9600 5200 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5200 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5200 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5200 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5200 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5200 50  0001 C CNN "Supplier P/N"
	1    9600 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5100 9850 5100
$Comp
L Device:R_Small R8
U 1 1 62761A3A
P 9600 5100
F 0 "R8" V 9550 5200 20  0000 C CNN
F 1 "1k" V 9600 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5100 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5100 50  0001 C CNN
F 4 "$0.1" H 9600 5100 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5100 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5100 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5100 50  0001 C CNN "Supplier P/N"
	1    9600 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5000 9850 5000
$Comp
L Device:R_Small R7
U 1 1 62761A2E
P 9600 5000
F 0 "R7" V 9550 5100 20  0000 C CNN
F 1 "1k" V 9600 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5000 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5000 50  0001 C CNN
F 4 "$0.1" H 9600 5000 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5000 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5000 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5000 50  0001 C CNN "Supplier P/N"
	1    9600 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 4900 9850 4900
$Comp
L Device:R_Small R6
U 1 1 62761A22
P 9600 4900
F 0 "R6" V 9550 5000 20  0000 C CNN
F 1 "1k" V 9600 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 4900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 4900 50  0001 C CNN
F 4 "$0.1" H 9600 4900 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 4900 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 4900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 4900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 4900 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 4900 50  0001 C CNN "Supplier P/N"
	1    9600 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 4800 9850 4800
$Comp
L Device:R_Small R5
U 1 1 62761A16
P 9600 4800
F 0 "R5" V 9550 4900 20  0000 C CNN
F 1 "1k" V 9600 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 4800 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 4800 50  0001 C CNN
F 4 "$0.1" H 9600 4800 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 4800 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 4800 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 4800 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 4800 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 4800 50  0001 C CNN "Supplier P/N"
	1    9600 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 4700 9850 4700
$Comp
L Device:R_Small R4
U 1 1 62761A0A
P 9600 4700
F 0 "R4" V 9550 4800 20  0000 C CNN
F 1 "1k" V 9600 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 4700 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 4700 50  0001 C CNN
F 4 "$0.1" H 9600 4700 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 4700 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 4700 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 4700 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 4700 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 4700 50  0001 C CNN "Supplier P/N"
	1    9600 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 4600 9850 4600
$Comp
L Device:R_Small R3
U 1 1 627619FE
P 9600 4600
F 0 "R3" V 9550 4700 20  0000 C CNN
F 1 "1k" V 9600 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 4600 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 4600 50  0001 C CNN
F 4 "$0.1" H 9600 4600 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 4600 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 4600 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 4600 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 4600 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 4600 50  0001 C CNN "Supplier P/N"
	1    9600 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 6200 9850 6200
$Comp
L Device:R_Small R11
U 1 1 627573FB
P 9600 6200
F 0 "R11" V 9550 6300 20  0000 C CNN
F 1 "1k" V 9600 6200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 6200 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 6200 50  0001 C CNN
F 4 "$0.1" H 9600 6200 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 6200 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 6200 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 6200 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 6200 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 6200 50  0001 C CNN "Supplier P/N"
	1    9600 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 6100 9850 6100
$Comp
L Device:R_Small R12
U 1 1 627573EF
P 9600 6100
F 0 "R12" V 9550 6200 20  0000 C CNN
F 1 "1k" V 9600 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 6100 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 6100 50  0001 C CNN
F 4 "$0.1" H 9600 6100 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 6100 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 6100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 6100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 6100 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 6100 50  0001 C CNN "Supplier P/N"
	1    9600 6100
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 6000 9850 6000
$Comp
L Device:R_Small R13
U 1 1 627573E3
P 9600 6000
F 0 "R13" V 9550 6100 20  0000 C CNN
F 1 "1k" V 9600 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 6000 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 6000 50  0001 C CNN
F 4 "$0.1" H 9600 6000 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 6000 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 6000 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 6000 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 6000 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 6000 50  0001 C CNN "Supplier P/N"
	1    9600 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5900 9850 5900
$Comp
L Device:R_Small R14
U 1 1 627573D7
P 9600 5900
F 0 "R14" V 9550 6000 20  0000 C CNN
F 1 "1k" V 9600 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5900 50  0001 C CNN
F 4 "$0.1" H 9600 5900 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5900 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5900 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5900 50  0001 C CNN "Supplier P/N"
	1    9600 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5800 9850 5800
$Comp
L Device:R_Small R15
U 1 1 6274E34C
P 9600 5800
F 0 "R15" V 9550 5900 20  0000 C CNN
F 1 "1k" V 9600 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5800 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5800 50  0001 C CNN
F 4 "$0.1" H 9600 5800 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5800 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5800 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5800 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5800 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5800 50  0001 C CNN "Supplier P/N"
	1    9600 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5700 9850 5700
$Comp
L Device:R_Small R16
U 1 1 6274E340
P 9600 5700
F 0 "R16" V 9550 5800 20  0000 C CNN
F 1 "1k" V 9600 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5700 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5700 50  0001 C CNN
F 4 "$0.1" H 9600 5700 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5700 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5700 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5700 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5700 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5700 50  0001 C CNN "Supplier P/N"
	1    9600 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5600 9850 5600
$Comp
L Device:R_Small R17
U 1 1 62745C0B
P 9600 5600
F 0 "R17" V 9550 5700 20  0000 C CNN
F 1 "1k" V 9600 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5600 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5600 50  0001 C CNN
F 4 "$0.1" H 9600 5600 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5600 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5600 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5600 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5600 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5600 50  0001 C CNN "Supplier P/N"
	1    9600 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 5500 9850 5500
$Comp
L Device:R_Small R18
U 1 1 627354E6
P 9600 5500
F 0 "R18" V 9550 5600 20  0000 C CNN
F 1 "1k" V 9600 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9600 5500 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1623327&DocType=Customer+Drawing&DocLang=English" H 9600 5500 50  0001 C CNN
F 4 "$0.1" H 9600 5500 50  0001 C CNN "Item Cost"
F 5 "CRG0805J1K0" H 9600 5500 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 9600 5500 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9600 5500 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805J1K0/2390009" H 9600 5500 50  0001 C CNN "Supplier Link"
F 9 "A126391DKR-ND" H 9600 5500 50  0001 C CNN "Supplier P/N"
	1    9600 5500
	0    1    1    0   
$EndComp
Text Label 12050 5000 2    50   ~ 0
PS_RST
Text Label 12050 5100 2    50   ~ 0
MON_RX
Text Label 12050 5200 2    50   ~ 0
SENSE_SCL
Text Label 12050 5300 2    50   ~ 0
RX_ZYNQ
Text Label 8850 5300 0    50   ~ 0
RX_IPMC
Text Label 8850 5200 0    50   ~ 0
TDI
Text Label 8850 5100 0    50   ~ 0
TCK
Text Label 8850 5000 0    50   ~ 0
EN
Text Label 8850 4900 0    50   ~ 0
CPLD_GPIO1
Text Label 8850 4800 0    50   ~ 0
CPLD_GPIO0
Text Label 8850 4700 0    50   ~ 0
FPGA_GPIO2
Text Label 8850 4600 0    50   ~ 0
FPGA_GPIO1
Text Label 8850 6200 0    50   ~ 0
FPGA_GPIO0
Text Label 8850 6100 0    50   ~ 0
PWR_EN
Text Label 8850 6000 0    50   ~ 0
SENSE_SDA
Text Label 8850 5900 0    50   ~ 0
TX_ZYNQ
Text Label 8850 5800 0    50   ~ 0
TX_IPMC
Text Label 8850 5700 0    50   ~ 0
TDO
Text Label 8850 5600 0    50   ~ 0
TMS
Text Label 8850 5500 0    50   ~ 0
PWR_GOOD
$Comp
L power:GND #PWR024
U 1 1 625B6DCC
P 10450 6600
F 0 "#PWR024" H 10450 6350 50  0001 C CNN
F 1 "GND" H 10455 6427 50  0000 C CNN
F 2 "" H 10450 6600 50  0001 C CNN
F 3 "" H 10450 6600 50  0001 C CNN
	1    10450 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 6600 10450 6500
Text Label 15150 3700 2    50   ~ 0
MISO
Text Label 15150 3800 2    50   ~ 0
MOSI
Text Label 15150 3900 2    50   ~ 0
SCK
Wire Wire Line
	14900 4000 15150 4000
Wire Wire Line
	14900 3900 15150 3900
Wire Wire Line
	14900 3800 15150 3800
Wire Wire Line
	14900 3700 15150 3700
Text Label 9850 4000 2    50   ~ 0
MISO
Text Label 9850 3900 2    50   ~ 0
MOSI
Text Label 9850 3800 2    50   ~ 0
SCK
Wire Wire Line
	7950 3800 7950 4450
Connection ~ 8150 4650
Connection ~ 8150 3900
Wire Wire Line
	8150 3900 8150 4650
Connection ~ 8050 4550
Connection ~ 8050 4000
Wire Wire Line
	8050 4550 8050 4000
Wire Wire Line
	7550 4450 7950 4450
Wire Wire Line
	7550 4550 8050 4550
Wire Wire Line
	7550 4650 8150 4650
Wire Wire Line
	7950 3800 9850 3800
Wire Wire Line
	8150 3900 9850 3900
Wire Wire Line
	8050 4000 9850 4000
Wire Wire Line
	10350 2150 10350 2100
Wire Wire Line
	10350 2400 10350 2500
Wire Wire Line
	10350 2400 10350 2350
Connection ~ 10350 2400
Text Label 11500 5600 2    50   ~ 0
USB_IN
Text Label 11500 5500 2    50   ~ 0
USB_OUT
Wire Wire Line
	7550 6700 8250 6700
Wire Wire Line
	7550 6600 8150 6600
Wire Wire Line
	7550 6500 8050 6500
Wire Wire Line
	7550 6400 7950 6400
Wire Wire Line
	5950 6200 6350 6200
Wire Wire Line
	5950 6900 6350 6900
Wire Wire Line
	5850 6800 6350 6800
Wire Wire Line
	4650 6700 6350 6700
Wire Wire Line
	4650 6600 6350 6600
Wire Wire Line
	5850 6300 6350 6300
Wire Wire Line
	5750 6400 6350 6400
Wire Wire Line
	5650 6500 6350 6500
$Comp
L power:GND #PWR021
U 1 1 628A9C21
P 7500 5900
F 0 "#PWR021" H 7500 5650 50  0001 C CNN
F 1 "GND" H 7505 5727 50  0000 C CNN
F 2 "" H 7500 5900 50  0001 C CNN
F 3 "" H 7500 5900 50  0001 C CNN
	1    7500 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 628A9C16
P 7300 5900
F 0 "C3" V 7150 5900 50  0000 C CNN
F 1 "25" V 7200 6050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7300 5900 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21B104KACNNNC_Spec.pdf" H 7300 5900 50  0001 C CNN
F 4 "$0.1" H 7300 5900 50  0001 C CNN "Item Cost"
F 5 "CL21B104KACNNNC" H 7300 5900 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 7300 5900 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7300 5900 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B104KACNNNC/3886757" H 7300 5900 50  0001 C CNN "Supplier Link"
F 9 "1276-1099-6-ND" H 7300 5900 50  0001 C CNN "Supplier P/N"
	1    7300 5900
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR017
U 1 1 628A9C0C
P 7000 5700
F 0 "#PWR017" H 7000 5550 50  0001 C CNN
F 1 "+3.3V" H 7000 5850 50  0000 C CNN
F 2 "" H 7000 5700 50  0001 C CNN
F 3 "" H 7000 5700 50  0001 C CNN
	1    7000 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 5750 7000 5700
$Comp
L power:GNDD #PWR018
U 1 1 6247EA3A
P 7150 7100
F 0 "#PWR018" H 7150 6850 50  0001 C CNN
F 1 "GNDD" H 7154 6945 50  0000 C CNN
F 2 "" H 7150 7100 50  0001 C CNN
F 3 "" H 7150 7100 50  0001 C CNN
	1    7150 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR012
U 1 1 62472B18
P 6850 7100
F 0 "#PWR012" H 6850 6850 50  0001 C CNN
F 1 "GNDA" H 6855 6927 50  0000 C CNN
F 2 "" H 6850 7100 50  0001 C CNN
F 3 "" H 6850 7100 50  0001 C CNN
	1    6850 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5350 5650 6500
Wire Wire Line
	5750 5250 5750 6400
Wire Wire Line
	5850 5150 5850 6300
Wire Wire Line
	5950 5050 5950 6200
Wire Wire Line
	7950 4450 7950 6400
Wire Wire Line
	8050 4550 8050 6500
Wire Wire Line
	8150 4650 8150 6600
Wire Wire Line
	5950 2800 5950 4150
Wire Wire Line
	5850 2700 5850 4050
Wire Wire Line
	5750 2600 5750 3950
Wire Wire Line
	5650 2500 5650 3850
Wire Wire Line
	5550 2400 5550 3750
Wire Wire Line
	5450 2300 5450 3650
Wire Wire Line
	5350 2200 5350 3550
Wire Wire Line
	5250 2100 5250 3450
Wire Wire Line
	8050 2400 8050 4000
Wire Wire Line
	8150 2500 8150 3900
$Comp
L Analog_ADC:MCP3208 U3
U 1 1 623BA8D2
P 6950 6500
F 0 "U3" H 6500 7050 50  0000 C CNN
F 1 "MCP3208" H 6600 6950 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 7050 6600 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21298c.pdf" H 7050 6600 50  0001 C CNN
F 4 "$6.25" H 6950 6500 50  0001 C CNN "Item Cost"
F 5 "MCP3208-BI/P" H 6950 6500 50  0001 C CNN "MFN"
F 6 "Microchip Technology" H 6950 6500 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 6950 6500 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/microchip-technology/MCP3208-BI-P/319443" H 6950 6500 50  0001 C CNN "Supplier Link"
F 9 "MCP3208-BI/P-ND" H 6950 6500 50  0001 C CNN "Supplier P/N"
	1    6950 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 6000 6850 5750
Wire Wire Line
	6850 5750 7000 5750
Wire Wire Line
	7150 5750 7150 5900
Connection ~ 7000 5750
Wire Wire Line
	7000 5750 7150 5750
Wire Wire Line
	7150 5900 7200 5900
Connection ~ 7150 5900
Wire Wire Line
	7150 5900 7150 6000
Wire Wire Line
	7400 5900 7500 5900
$Comp
L power:GND #PWR0101
U 1 1 6297CAC6
P 7500 3950
F 0 "#PWR0101" H 7500 3700 50  0001 C CNN
F 1 "GND" H 7505 3777 50  0000 C CNN
F 2 "" H 7500 3950 50  0001 C CNN
F 3 "" H 7500 3950 50  0001 C CNN
	1    7500 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 6297CADA
P 7000 3750
F 0 "#PWR0102" H 7000 3600 50  0001 C CNN
F 1 "+3.3V" H 7000 3900 50  0000 C CNN
F 2 "" H 7000 3750 50  0001 C CNN
F 3 "" H 7000 3750 50  0001 C CNN
	1    7000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3800 7000 3750
Wire Wire Line
	6850 4050 6850 3800
Wire Wire Line
	6850 3800 7000 3800
Wire Wire Line
	7150 3800 7150 3950
Connection ~ 7000 3800
Wire Wire Line
	7000 3800 7150 3800
Wire Wire Line
	7150 3950 7200 3950
Connection ~ 7150 3950
Wire Wire Line
	7150 3950 7150 4050
Wire Wire Line
	7400 3950 7500 3950
$Comp
L Device:C_Small C2
U 1 1 6288D24F
P 7300 3950
F 0 "C2" V 7150 3950 50  0000 C CNN
F 1 "25" V 7200 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7300 3950 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21B104KACNNNC_Spec.pdf" H 7300 3950 50  0001 C CNN
F 4 "$0.1" H 7300 3950 50  0001 C CNN "Item Cost"
F 5 "CL21B104KACNNNC" H 7300 3950 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 7300 3950 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7300 3950 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B104KACNNNC/3886757" H 7300 3950 50  0001 C CNN "Supplier Link"
F 9 "1276-1099-6-ND" H 7300 3950 50  0001 C CNN "Supplier P/N"
	1    7300 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 2300 7950 3800
$Comp
L Analog_ADC:MCP3208 U1
U 1 1 623B7303
P 6950 2400
F 0 "U1" H 6500 2950 50  0000 C CNN
F 1 "MCP3208" H 6600 2850 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 7050 2500 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21298c.pdf" H 7050 2500 50  0001 C CNN
F 4 "$6.25" H 6950 2400 50  0001 C CNN "Item Cost"
F 5 "MCP3208-BI/P" H 6950 2400 50  0001 C CNN "MFN"
F 6 "Microchip Technology" H 6950 2400 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 6950 2400 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/microchip-technology/MCP3208-BI-P/319443" H 6950 2400 50  0001 C CNN "Supplier Link"
F 9 "MCP3208-BI/P-ND" H 6950 2400 50  0001 C CNN "Supplier P/N"
	1    6950 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 628F6900
P 7300 1800
F 0 "C1" V 7150 1800 50  0000 C CNN
F 1 "25" V 7200 1950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7300 1800 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL21B104KACNNNC_Spec.pdf" H 7300 1800 50  0001 C CNN
F 4 "$0.1" H 7300 1800 50  0001 C CNN "Item Cost"
F 5 "CL21B104KACNNNC" H 7300 1800 50  0001 C CNN "MFN"
F 6 "Samsung Electro-Mechanics" H 7300 1800 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 7300 1800 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B104KACNNNC/3886757" H 7300 1800 50  0001 C CNN "Supplier Link"
F 9 "1276-1099-6-ND" H 7300 1800 50  0001 C CNN "Supplier P/N"
	1    7300 1800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 629EC8DB
P 7500 1800
F 0 "#PWR0103" H 7500 1550 50  0001 C CNN
F 1 "GND" H 7505 1627 50  0000 C CNN
F 2 "" H 7500 1800 50  0001 C CNN
F 3 "" H 7500 1800 50  0001 C CNN
	1    7500 1800
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 629EC8E5
P 7000 1600
F 0 "#PWR0104" H 7000 1450 50  0001 C CNN
F 1 "+3.3V" H 7000 1750 50  0000 C CNN
F 2 "" H 7000 1600 50  0001 C CNN
F 3 "" H 7000 1600 50  0001 C CNN
	1    7000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1650 7000 1600
Wire Wire Line
	6850 1900 6850 1650
Wire Wire Line
	6850 1650 7000 1650
Wire Wire Line
	7150 1650 7150 1800
Connection ~ 7000 1650
Wire Wire Line
	7000 1650 7150 1650
Wire Wire Line
	7150 1800 7200 1800
Connection ~ 7150 1800
Wire Wire Line
	7150 1800 7150 1900
Wire Wire Line
	7400 1800 7500 1800
$Comp
L power:+3.3V #PWR0105
U 1 1 625DC94E
P 8650 2900
F 0 "#PWR0105" H 8650 2750 50  0001 C CNN
F 1 "+3.3V" H 8650 3050 50  0000 C CNN
F 2 "" H 8650 2900 50  0001 C CNN
F 3 "" H 8650 2900 50  0001 C CNN
	1    8650 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2600 8450 2600
Wire Wire Line
	7550 2500 8150 2500
Wire Wire Line
	7550 2400 8050 2400
Wire Wire Line
	7550 2300 7950 2300
$Comp
L power:GND #PWR0106
U 1 1 626293BE
P 9450 2600
F 0 "#PWR0106" H 9450 2350 50  0001 C CNN
F 1 "GND" H 9455 2427 50  0000 C CNN
F 2 "" H 9450 2600 50  0001 C CNN
F 3 "" H 9450 2600 50  0001 C CNN
	1    9450 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	8250 6700 8250 4100
Wire Wire Line
	8250 4100 9850 4100
Wire Wire Line
	7550 4750 8350 4750
Wire Wire Line
	8350 4750 8350 4200
Wire Wire Line
	8350 4200 9850 4200
Wire Wire Line
	9850 4300 8450 4300
Wire Wire Line
	8450 2600 8450 4300
Wire Wire Line
	8650 2900 8950 2900
Wire Wire Line
	11050 6200 11400 6200
Wire Wire Line
	11050 6100 11400 6100
Wire Wire Line
	11050 6000 11400 6000
Wire Wire Line
	11050 5900 11400 5900
Wire Wire Line
	11050 5600 11500 5600
Wire Wire Line
	11050 4200 11350 4200
Wire Wire Line
	11050 5500 11500 5500
$Comp
L MCU_Microchip_ATmega:ATmega325PV-10AU U4
U 1 1 62324C02
P 10450 4500
F 0 "U4" H 9950 2450 50  0000 C CNN
F 1 "ATmega325PV-10AU" H 10000 2550 50  0000 C CNN
F 2 "Package_QFP:TQFP-64_14x14mm_P0.8mm" H 10450 4500 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8023.pdf" H 10450 4500 50  0001 C CNN
F 4 "$6.54" H 10450 4500 50  0001 C CNN "Item Cost"
F 5 "ATMEGA325PV-10AU" H 10450 4500 50  0001 C CNN "MFN"
F 6 "Microchip Technology" H 10450 4500 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 10450 4500 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/microchip-technology/ATMEGA325PV-10AU/1245852" H 10450 4500 50  0001 C CNN "Supplier Link"
F 9 "ATMEGA325PV-10AU-ND" H 10450 4500 50  0001 C CNN "Supplier P/N"
	1    10450 4500
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J5
U 1 1 625A9334
P 9150 3100
F 0 "J5" H 9200 3517 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 9200 3426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 9150 3100 50  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE122%20.100%20SBH11%20SERIES%20MALE%20BOX%20HDR%20ST%20RA%20SMT.pdf" H 9150 3100 50  0001 C CNN
F 4 "$0.42" H 9150 3100 50  0001 C CNN "Item Cost"
F 5 "SBH11-PBPC-D05-ST-BK" H 9150 3100 50  0001 C CNN "MFN"
F 6 "Sullins Connector Solutions" H 9150 3100 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 9150 3100 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/sullins-connector-solutions/SBH11-PBPC-D05-ST-BK/1990062" H 9150 3100 50  0001 C CNN "Supplier Link"
F 9 "S9169-ND" H 9150 3100 50  0001 C CNN "Supplier P/N"
	1    9150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 3500 9450 3500
Wire Wire Line
	9450 3500 9450 3300
Wire Wire Line
	9850 3400 8950 3400
Wire Wire Line
	8950 3400 8950 3300
Wire Wire Line
	9850 3300 9500 3300
Wire Wire Line
	9500 3300 9500 3200
Wire Wire Line
	9500 3200 9450 3200
Wire Wire Line
	9850 3200 9800 3200
Wire Wire Line
	9800 3200 9800 3450
Wire Wire Line
	9800 3450 8900 3450
Wire Wire Line
	8900 3450 8900 3200
Wire Wire Line
	8900 3200 8950 3200
Wire Wire Line
	9850 3100 9450 3100
Wire Wire Line
	9850 3000 9750 3000
Wire Wire Line
	9750 3000 9750 3550
Wire Wire Line
	9750 3550 8850 3550
Wire Wire Line
	8850 3550 8850 3100
Wire Wire Line
	8850 3100 8950 3100
Wire Wire Line
	9850 2900 9550 2900
Wire Wire Line
	9550 2900 9550 3000
Wire Wire Line
	9550 3000 9450 3000
Wire Wire Line
	9850 2800 9700 2800
Wire Wire Line
	9700 2800 9700 3600
Wire Wire Line
	9700 3600 8800 3600
Wire Wire Line
	8800 3600 8800 3000
Wire Wire Line
	8800 3000 8950 3000
Wire Wire Line
	9450 2600 9450 2900
$Comp
L power:GNDD #PWR0107
U 1 1 62E75560
P 10950 9250
F 0 "#PWR0107" H 10950 9000 50  0001 C CNN
F 1 "GNDD" H 10954 9095 50  0000 C CNN
F 2 "" H 10950 9250 50  0001 C CNN
F 3 "" H 10950 9250 50  0001 C CNN
	1    10950 9250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0108
U 1 1 62E75574
P 10450 9250
F 0 "#PWR0108" H 10450 9000 50  0001 C CNN
F 1 "GNDA" H 10455 9077 50  0000 C CNN
F 2 "" H 10450 9250 50  0001 C CNN
F 3 "" H 10450 9250 50  0001 C CNN
	1    10450 9250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 62E919B6
P 11450 9250
F 0 "#PWR0109" H 11450 9000 50  0001 C CNN
F 1 "GND" H 11455 9077 50  0000 C CNN
F 2 "" H 11450 9250 50  0001 C CNN
F 3 "" H 11450 9250 50  0001 C CNN
	1    11450 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 9250 10450 9150
Wire Wire Line
	10450 9150 10950 9150
Wire Wire Line
	11450 9150 11450 9250
Wire Wire Line
	10950 9150 10950 9250
Connection ~ 10950 9150
Wire Wire Line
	10950 9150 11450 9150
NoConn ~ 14650 5250
Wire Wire Line
	11600 5900 12000 5900
Wire Wire Line
	11600 6000 12000 6000
Wire Wire Line
	11600 6100 12000 6100
Wire Wire Line
	11600 6200 12000 6200
$Comp
L Device:R_Small R2
U 1 1 6264F101
P 5900 9150
F 0 "R2" V 5850 9250 20  0000 C CNN
F 1 "240" V 5900 9150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5900 9150 50  0001 C CNN
F 3 "https://industrial.panasonic.com/ww/products/pt/general-purpose-chip-resistors/models/ERJ6GEYJ241V" H 5900 9150 50  0001 C CNN
F 4 "$0.1" H 5900 9150 50  0001 C CNN "Item Cost"
F 5 "ERJ-6GEYJ241V" H 5900 9150 50  0001 C CNN "MFN"
F 6 "Panasonic Electronic Components" H 5900 9150 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 5900 9150 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/panasonic-electronic-components/ERJ-6GEYJ241V/89709" H 5900 9150 50  0001 C CNN "Supplier Link"
F 9 "P240ADKR-ND" H 5900 9150 50  0001 C CNN "Supplier P/N"
	1    5900 9150
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 9150 5050 9100
Wire Wire Line
	5050 8650 5050 8600
$Comp
L Device:R_Small R1
U 1 1 625F2D30
P 5900 8650
F 0 "R1" V 5850 8750 20  0000 C CNN
F 1 "2k" V 5900 8650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5900 8650 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1773204&DocType=DS&DocLang=English" H 5900 8650 50  0001 C CNN
F 4 "$0.1" H 5900 8650 50  0001 C CNN "Item Cost"
F 5 "CRG0805F2K0" H 5900 8650 50  0001 C CNN "MFN"
F 6 "TE Connectivity Passive Product" H 5900 8650 50  0001 C CNN "Manufacturer"
F 7 "Digi-Key" H 5900 8650 50  0001 C CNN "Supplier"
F 8 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805F2K0/2380881" H 5900 8650 50  0001 C CNN "Supplier Link"
F 9 "A126358DKR-ND" H 5900 8650 50  0001 C CNN "Supplier P/N"
	1    5900 8650
	0    1    1    0   
$EndComp
$EndSCHEMATC
